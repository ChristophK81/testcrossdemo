//============================================================================
// Name        : qlib.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <opencv2/opencv.hpp>

#include <src/Detector.h>

using namespace cv;
using namespace std;
using namespace qlib;

int main() {
	cout << "!!!Hello Lib World!!!" << endl; // prints !!!Hello World!!!

	qlib::Detector test;


	Mat inputImage = imread("/mnt/DEV/workspace/qlib_tester/src/20front.png");
	//imshow("Input Image", inputImage);


	test.setTemplate(inputImage);

	waitKey(0);

	return 0;
}

