//============================================================================
// Name        : qlibDevelopment.cpp
// Author      : CK
// Version     :
// Copyright   : 
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Detector.h"


#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace std;

using namespace qlib;

int main()
{
  Mat src, gray;
  src = imread( "/home/ck/Schreibtisch/matches_result.jpg", 1 );
  resize(src,src,Size(2160,2160 * 2160/ 3600));

  cvtColor( src, gray, CV_BGR2GRAY );

  // Reduce the noise so we avoid false circle detection
  GaussianBlur( gray, gray, Size(9, 9), 2, 2 );

  vector<Vec3f> circles;

  // Apply the Hough Transform to find the circles
  HoughCircles( gray, circles, CV_HOUGH_GRADIENT, 1, 1, 100, 50, 0, 0 );

  // Draw the circles detected
  for( size_t i = 0; i < circles.size(); i++ )
  {
      Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
      int radius = cvRound(circles[i][2]);
      circle( src, center, 3, Scalar(0,255,0), -1, 8, 0 );// circle center
      circle( src, center, radius, Scalar(0,0,255), 3, 8, 0 );// circle outline
      cout << "center : " << center << "\nradius : " << radius << endl;
   }

  // Show your results
  namedWindow( "Hough Circle Transform Demo", CV_WINDOW_AUTOSIZE );
  imshow( "Hough Circle Transform Demo", src );

  waitKey(0);
  return 0;
}

int main_() {
			cout << "!!!Go!!!" << endl; // prints !!!Hello World!!!

			clock_t start = clock();

			Detector test;
			//Mat templateImage = imread("/mnt/DEV/workspace/qlibDevelopment/res/20front.png",IMREAD_GRAYSCALE);
			Mat templateImage = imread("/mnt/DEV/workspace/qlibDevelopment/res/20front_wo_serial.png",IMREAD_GRAYSCALE);
			//blur( templateImage, templateImage, Size(3,3) );
			Mat image;


			Mat inputImage = imread("/mnt/DEV/workspace/qlibDevelopment/res/IMG_20150930_153853_smaller.jpg",IMREAD_GRAYSCALE);
			//Mat inputImage = imread("/mnt/DEV/workspace/qlibDevelopment/res/IMG_20150930_154020.jpg");

			//test.setTemplate(templateImage);
			//Mat ret = test.process(inputImage);

			clock_t end = clock();
			double time = (double) (end-start) / CLOCKS_PER_SEC * 1000.0;

			cout << "time" << endl;
			cout << time << endl;
			//imshow("rest", ret);
			waitKey(0);
	return 0;
}
