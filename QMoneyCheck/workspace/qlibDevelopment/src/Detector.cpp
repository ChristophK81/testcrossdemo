/*
 * Detector.cpp
 *
 *  Created on: 29.09.2015
 *      Author: ck
 */

#include "Detector.h"

namespace qlib {

Detector::Detector() {
	detector = ORB::create();
	detector->setMaxFeatures(1000);
	matcher = DescriptorMatcher::create("BruteForce-Hamming");
}

Detector::~Detector() {
	// TODO Auto-generated destructor stub
}


void Detector::setTemplate(const Mat image)
{
	templateFrame = image;

    detector->detectAndCompute(image, noArray(),  templateKp, templateDesc);

    //std::vector<cv::Point2f> templateBb(4);
    templateBb.push_back(cv::Point(0,0));
    templateBb.push_back(cv::Point(image.cols,0));
    templateBb.push_back(cv::Point(image.cols,image.rows));
    templateBb.push_back(cv::Point(0,image.rows));
    Mat res;
    drawKeypoints(image,templateKp,res,Scalar(255, 0, 0));
    imshow("templateImage", res);
    waitKey(0);
}

Mat Detector::process(const Mat frame){
	vector<KeyPoint> kp;
	Mat desc;
	detector->detectAndCompute(frame, noArray(), kp, desc);

	 Mat res1;
	 drawKeypoints(frame,kp,res1,Scalar(255, 0, 0));
	 imshow("templateImage", res1);
	 waitKey(0);

	vector< vector<DMatch> > matches;
	vector<KeyPoint> matched1, matched2;

	matcher->knnMatch(templateDesc, desc, matches, 2);
	for(unsigned i = 0; i < matches.size(); i++) {
		if(matches[i][0].distance < nnMatchRatio * matches[i][1].distance) {
			matched1.push_back(templateKp[matches[i][0].queryIdx]);
			matched2.push_back(kp[matches[i][0].trainIdx]);
		}
	}

	cout << "matched: " << matched1.size() << endl;

    Mat inlier_mask, homography;
    vector<KeyPoint> inliers1, inliers2;
    vector<DMatch> inlier_matches;
    if(matched1.size() >= 4) {
        homography = findHomography(Points(matched1), Points(matched2),  RANSAC, ransacThresh, inlier_mask);
    }


    Mat matrixR;
    Mat matrixQ;
    Mat matrixQx;
    Mat matrixQy;
    Mat matrixQz;

    Vec3d test = RQDecomp3x3(homography, matrixR,  matrixQ,  matrixQx,  matrixQy,  matrixQz);
    /*
    cout << "test" << endl;
    cout <<test << endl;
    cout << "matrixR" << endl;
	cout << matrixR << endl;
	cout << "matrixQ" << endl;
	cout << matrixQ << endl;
    cout << "matrixQx" << endl;
    cout << matrixQx << endl;
    cout << "matrixQy" << endl;
    cout << matrixQy << endl;
    cout << "matrixQz" << endl;


    cout << matrixQz << endl;
*/

    if(matched1.size() < 4 || homography.empty()) {
        Mat res;
        return res;
    }

    for(unsigned i = 0; i < matched1.size(); i++) {
        if(inlier_mask.at<uchar>(i)) {
            int new_i = static_cast<int>(inliers1.size());
            inliers1.push_back(matched1[i]);
            inliers2.push_back(matched2[i]);
            inlier_matches.push_back(DMatch(new_i, new_i, 0));
        }
    }

    cout << "inliers: " << inliers1.size() << endl;

    vector<Point2f> new_bb;
    perspectiveTransform(templateBb, new_bb, homography);
    Mat frame_with_bb = frame.clone();
    if( (int)inliers1.size() >= bbMinInliers ) {
        drawBoundingBox(frame_with_bb, new_bb);
    }

    Mat res;

    drawMatches(templateFrame, inliers1, frame_with_bb, inliers2,
                inlier_matches, res,
                Scalar(255, 0, 0), Scalar(255, 0, 0));

    return res;

}

void Detector::drawBoundingBox(Mat image, vector<Point2f> bb)
{
    for(unsigned i = 0; i < bb.size() - 1; i++) {
        line(image, bb[i], bb[i + 1], Scalar(0, 0, 255), 2);
    }
    line(image, bb[bb.size() - 1], bb[0], Scalar(0, 0, 255), 2);
}

vector<Point2f> Detector::Points(vector<KeyPoint> keypoints)
{
    vector<Point2f> res;
    for(unsigned i = 0; i < keypoints.size(); i++) {
        res.push_back(keypoints[i].pt);
    }
    return res;
}

} /* namespace qlib */
