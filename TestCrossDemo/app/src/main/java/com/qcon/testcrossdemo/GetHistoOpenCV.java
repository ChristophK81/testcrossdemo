package com.qcon.testcrossdemo;


import android.util.Log;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by ElieB on 7/16/2015.
 */
public class GetHistoOpenCV {



    public static final String TAG = GetHistoOpenCV.class.getSimpleName();

    public static ArrayList<Group> findAllMax(Mat histogram){

        ArrayList<Group> groups = new ArrayList<Group>();
        int valuePrev = -1;
        int valueNext = -1;
        int r = histogram.height();
        Log.d("the histogram height is: " + r, " ");


        for (int y = 0; y < 255; y++){
            //Log.d("y is ", " " + y+ "  " + (int)histogram.get(y,0)[0] );
            int value = (int)histogram.get(y,0) [0];

            switch (y){
                case 0:
                    valueNext = (int) histogram.get(y + 1, 0)[0];
                    break;

                case 254:
                    valuePrev = (int) histogram.get(y - 1, 0)[0];
                    break;

                default:
                    valueNext = (int) histogram.get(y + 1, 0)[0];
                    valuePrev = (int) histogram.get(y - 1, 0)[0];
                    break;
            }

            if ((y == 0 && value > valueNext) || (value > valuePrev &&  value > valueNext) || (y == histogram.height() && value > valuePrev)) {

                Group group = new Group();
                group.setIdx(y);
                group.setValue(value);

                findMins(histogram, group);
                groups.add(group);
            }
        }
        return groups;
    }

    private static Peaks findMins(Mat histogram, Group group) {
        ArrayList<Integer> minValues = new ArrayList<Integer>();
        ArrayList<Integer> minIndices = new ArrayList<Integer>();
        Peaks valleys = new Peaks(minIndices, minValues);

        boolean done = false;

        for (int y = group.getIdx()+1; y < histogram.height() && !done; y++){
            Log.d("y is ", " " + y+ "  " + (int)histogram.get(y,0)[0] );
            int value = (int)histogram.get(y,0) [0];
            int valueNext = (y < histogram.height()-1) ? (int) histogram.get(y + 1,0) [0] : Integer.MAX_VALUE;

            if (value < valueNext) {
                group.setNextIdx(y);
                group.setNextValue(value);
                done =  true;
            }
        }

        done = false;

        for (int y = group.getIdx()-1; y >= 0 && !done; y--){
            Log.d("y is ", " " + y+ "  " + (int)histogram.get(y,0)[0] );

            int value       = (int) histogram.get(y,0) [0];
            int valuePrev   = y > 0 ? (int) histogram.get(y-1, 0) [0] : Integer.MAX_VALUE ;

            if (value < valuePrev ) {
                group.setPrevIdx(y);
                group.setPrevValue(value);
                done =  true;
            }
        }

        return valleys;
    }


    public static Mat makeHist(Mat img, final int colorNum) {

        List<Mat> matList = new LinkedList<Mat>();
        matList.add(img);


        //separate the colors
        final List<Mat> bgrPlanes = new ArrayList<Mat>();
        Core.split(img, bgrPlanes);

        Mat hist = new Mat();

        MatOfFloat ranges=new MatOfFloat(0,256);
        MatOfInt histSizeMat = new MatOfInt(255);

        ArrayList<Mat> channel = new ArrayList<Mat>(){{
            add(bgrPlanes.get(colorNum));
        }};
        Imgproc.calcHist(channel, new MatOfInt(0), new Mat(), hist, histSizeMat, ranges);
        Core.normalize(hist, hist, 0, hist.rows(),Core.NORM_MINMAX, -1, new Mat());

        return hist;
    }
}



