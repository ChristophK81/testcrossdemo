package com.qcon.testcrossdemo;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.os.Environment;
import android.support.annotation.NonNull;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.utils.Converters;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.Vector;

/**
 * Created by ck on 29.08.15.
 */
public class CrossDetetctor {

    private boolean writeDebugInformation = true;
    private boolean writeDebugImage = true;

    private Mat templatePointsInt = null;
    private Mat templateImage = null;
    private Mat templatePoints = null;

    private Context context = null;

    private File path = null;
    private File logPath = null;

    private XMLConfig xmlConfig = null;

    private int detectionMethode = 1;
    private int sizeTemplate = 1;

    public CrossDetetctor(Context context, int methode, int sizeTemplate, boolean writeDebugInformation,boolean writeDebugImage){

        this.context = context;
        this.detectionMethode = methode;

        this.writeDebugInformation = writeDebugInformation;
        this.writeDebugImage = writeDebugImage;
        try {
            templateImage = Utils.loadResource(context, R.drawable.cross, 1);
        } catch (java.io.IOException e) {
        }

        Mat imgSource = new Mat();
        Size sz = new Size(sizeTemplate, sizeTemplate);
        Imgproc.resize(templateImage, imgSource, sz);
        templateImage = imgSource;

        this.sizeTemplate = sizeTemplate;
        path = getLocalStorageDir("TestCrossDemo");
        xmlConfig = new XMLConfig(context, path);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        String currentDateandTime = sdf.format(new Date());
        logPath = getLocalStorageDir("TestCrossDemo/Log_" + currentDateandTime);

    }


    public int analyse(Mat sceneImage) {

        try {
            return this.findConture(sceneImage) ;
        } catch(Exception e){
            addToLog(e.getMessage());
            addToLog(e.getLocalizedMessage());

            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            addToLog(sw.toString());

            return -2;
        }

    }
    /**
     * main function - Main function - extracted contours and tested it against the template
     *
     * @param sceneImage - Mat - scene image (BGR)
     * @return - int - 1: found marked object - 0:found object without marker - -1:no object found
     */
    public int findConture(Mat sceneImage) {

        addToLog(String.format("Start with Method %d",detectionMethode));

        MatOfPoint templateConture = getTemplateConture(templateImage);

        Mat imgSource = sceneImage.clone();

        if (writeDebugImage)
            SaveJpeg(sceneImage, "scene");

        //convert the image to black and white
        Imgproc.cvtColor(imgSource, imgSource, Imgproc.COLOR_BGR2GRAY);
        Size sceneSize = imgSource.size();
        int factor = (int)sceneSize.width/500;
        if (factor < 1) factor = 1;

        Imgproc.resize(imgSource, imgSource, new Size(sceneSize.width / factor, sceneSize.height / factor));

        Imgproc.GaussianBlur(imgSource, imgSource, new org.opencv.core.Size(5, 5), 5);

        //convert the image to black and white does (8 bit)
        Imgproc.Canny(imgSource, imgSource, 50, 50);
        if (writeDebugImage)
            SaveImage(imgSource, "SceneCanny");

        //apply gaussian blur to smoothen lines of dots
        Imgproc.GaussianBlur(imgSource, imgSource, new org.opencv.core.Size(5, 5), 5);
        if (writeDebugImage)
            SaveImage(imgSource, "SceneBlur");

        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        List<MatOfPoint> approxContours = new ArrayList<MatOfPoint>();
        List<MatOfPoint2f> approxContours2f = new ArrayList<MatOfPoint2f>();
        List<Double> area = new ArrayList<Double>();

        //find the contours
        Imgproc.findContours(imgSource, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

        double minAreaSize = (imgSource.cols() * imgSource.rows()) * 0.01;
        double minValue = Double.MAX_VALUE;
        double matchValue = Double.MAX_VALUE;
        int bestContureIdx = -1;

        if (writeDebugInformation)
            addToLog(String.format("minAreaSize - %f = x:%d * y:%d", minAreaSize, imgSource.cols(), imgSource.rows()));

        for (int idx = 0; idx < contours.size(); idx++) {

            MatOfPoint2f tmpContours2f = new MatOfPoint2f();
            MatOfPoint2f tmpApprox2f = new MatOfPoint2f();
            MatOfPoint tmpApprox = new MatOfPoint();

            contours.get(idx).convertTo(tmpContours2f, CvType.CV_32FC2);
            double minX=Double.MAX_VALUE,minY=Double.MAX_VALUE;
            double maxX=Double.MIN_VALUE,maxY=Double.MIN_VALUE;
            double[] tmpPoint;

            for (int j = 0;j< tmpContours2f.rows();j++){
                tmpPoint = tmpContours2f.get(j,0);
                if (tmpPoint[0] < minX)
                    minX = tmpPoint[0];
                else if (tmpPoint[0] > maxX)
                    maxX = tmpPoint[0];

                if (tmpPoint[1] < minY)
                    minY = tmpPoint[1];
                if (tmpPoint[1] > maxY)
                    maxY = tmpPoint[1];
            }
            int maxDiffrence = 0;
            if (maxX-minX < maxY-minY) {
                maxDiffrence = (int)((maxX-minX)/30);
            }else{
                maxDiffrence = (int)((maxY-minY)/30);
            }

            Imgproc.approxPolyDP(tmpContours2f, tmpApprox2f, 1+maxDiffrence, true);
            tmpApprox2f.convertTo(tmpApprox, CvType.CV_32S);
            approxContours.add(tmpApprox);
            approxContours2f.add(tmpApprox2f);

            area.add(Imgproc.contourArea(tmpApprox));

            if (area.get(idx) > minAreaSize) {
                if (writeDebugInformation)
                    addToLog(String.format("area.get(%d):%f - size:%f ", idx, area.get(idx), tmpApprox.size().height));

                if (tmpApprox.size().height == 12) {

                    matchValue = Imgproc.matchShapes(templateConture, tmpApprox, Imgproc.CV_CONTOURS_MATCH_I3, 0);

                    if (matchValue > 0.0 && matchValue < minValue) {
                        bestContureIdx = idx;
                        minValue = matchValue;
                    }
                }
            }
        }

        if (bestContureIdx > -1) {
            MatOfPoint2f downscaled = approxContours2f.get(bestContureIdx);
            ArrayList<Point> upscaled = new ArrayList<Point>();
            double[] point;
            for (int idx = 0; idx < downscaled.rows(); idx++) {
                point = downscaled.get(idx,0);
                point[0] = point[0] * factor;
                point[1] = point[1] * factor;
                upscaled.add(new Point(point));
            }

            MatOfPoint2f approxContourUpscaled = new MatOfPoint2f();
            approxContourUpscaled.fromList(upscaled);

            return analyseMatch(sceneImage,approxContourUpscaled);

        }

        addToLog("NO CROSS(Conture) found");

        MediaScannerConnection.scanFile(context, new String[]{logPath.getAbsolutePath().toString()}, null, null);

        return -1;
    }

    //public  Point intersectLines(Line2D l, Line2D m)throws IllegalArgumentException
    public double[][] intersectLines(Point l1, Point l2,Point m1, Point m2) throws IllegalArgumentException
    {
        // Wegen der Lesbarkeit
        double x1 = l1.x;
        double x2 = l2.x;
        double x3 = m1.x;
        double x4 = m2.x;
        double y1 = l1.y;
        double y2 = l2.y;
        double y3 = m1.y;
        double y4 = m2.y;

        // Zaehler
        double zx = (x1 * y2 - y1 * x2)*(x3-x4) - (x1 - x2) * (x3 * y4 - y3 * x4);
        double zy = (x1 * y2 - y1 * x2)*(y3-y4) - (y1 - y2) * (x3 * y4 - y3 * x4);

        // Nenner
        double n = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);

        // Koordinaten des Schnittpunktes
        double x = zx/n;
        double y = zy/n;

        // Vielleicht ist bei der Division durch n etwas schief gelaufen
        if (Double.isNaN(x)& Double.isNaN(y))
        {
            throw new IllegalArgumentException("Schnittpunkt nicht eindeutig. ");
        }

        double[][] ret = {{x},{y}};
        return ret;
    }

    private Mat convexHullRect(MatOfPoint conture) {
        MatOfInt hullIndex = new MatOfInt();
        Imgproc.convexHull(conture, hullIndex, true);
        List<Integer> convexPoints = hullIndex.toList();
        List<Point> convexHullRect =  new ArrayList<Point>();
        Size s = new Size();
        s.height = 4;
        s.width = 2;
        Mat ret =  new Mat(s, CvType.CV_32FC1);

        Collections.sort(convexPoints);

        int i = 0;
        for(i = 0;i<convexPoints.size()-1;i++){
            if (Math.abs(convexPoints.get(i) - convexPoints.get(i+1)) == 1)
                break;
        }

        int startPoint = convexPoints.get(i);
        double[][] p;
        Point l1 = new Point(conture.get(startPoint,0));
        Point l2 = new Point(conture.get(startPoint+1,0));
        Point m1 = new Point(conture.get(startPoint+3,0));
        Point m2 = new Point(conture.get(startPoint+4,0));

        if (writeDebugInformation)
            writStringToFile("Points.txt", String.format("P1 %f/ %f - %f/ %f : %f/ %f - %f/ %f", l1.x, l1.y, l2.x, l2.y, m1.x, m1.y, m2.x, m2.y));

        p = intersectLines(l1, l2, m1, m2);
        ret.put(0,0, p[0]);
        ret.put(0,1, p[1]);

        l1 = new Point(conture.get(startPoint+6,0));
        l2 = new Point(conture.get(startPoint+7,0));
        m1 = new Point(conture.get(startPoint+3,0));
        m2 = new Point(conture.get(startPoint+4,0));

        if (writeDebugInformation)
            writStringToFile("Points.txt", String.format("P2 %f/ %f - %f/ %f : %f/ %f - %f/ %f", l1.x, l1.y, l2.x, l2.y, m1.x, m1.y, m2.x, m2.y));

        p =intersectLines(l1, l2, m1, m2);
        ret.put(1,0, p[0]);
        ret.put(1,1, p[1]);

        l1 = new Point(conture.get(startPoint+6,0));
        l2 = new Point(conture.get(startPoint+7,0));
        m1 = new Point(conture.get(startPoint+9,0));

        if (startPoint<2)
            m2 = new Point(conture.get(startPoint+10,0));
        else
            m2 = new Point(conture.get(0,0));

        if (writeDebugInformation)
            writStringToFile("Points.txt", String.format("P3 %f/ %f - %f/ %f : %f/ %f - %f/ %f", l1.x, l1.y, l2.x, l2.y, m1.x, m1.y, m2.x, m2.y));

        p =intersectLines(l1, l2, m1, m2);
        ret.put(2,0, p[0]);
        ret.put(2,1, p[1]);

        l1 = new Point(conture.get(startPoint,0));
        l2 = new Point(conture.get(startPoint+1,0));
        m1 = new Point(conture.get(startPoint+9,0));
        if (startPoint<2)
            m2 = new Point(conture.get(startPoint+10,0));
        else
            m2 = new Point(conture.get(0,0));

        if (writeDebugInformation)
            writStringToFile("Points.txt", String.format("P4 %f/ %f - %f/ %f : %f/ %f - %f/ %f", l1.x, l1.y, l2.x, l2.y, m1.x, m1.y, m2.x, m2.y));

        p =intersectLines(l1, l2, m1, m2);
        ret.put(3,0, p[0]);
        ret.put(3, 1, p[1]);

        return ret;
    }
    /**
     * This function compares the 5 places
     * the first (middle) agains the other 4 (left,right,upper,bottom)
     *
     * @param sceneImage      - Mat - scene image (BGR)
     * @param approxContour2f - MatOfPoint2f - approximated conture of matched object
     * @return - int - 1: found marked object - 0:found object without marker
     */
    private int analyseMatch(Mat sceneImage, MatOfPoint2f approxContour2f) {

        MatOfPoint approxContour = new MatOfPoint();
        approxContour2f.convertTo(approxContour, CvType.CV_32S);

        Mat scenePoints2f = convexHullRect(approxContour) ;
        Mat scenePoints = new Mat();

        //Imgproc.boxPoints(Imgproc.minAreaRect(approxContour2f), scenePoints2f);
        Mat perspectiveTransform = Imgproc.getPerspectiveTransform(scenePoints2f, templatePoints);

        Mat outputMat = templateImage.clone();
        Imgproc.warpPerspective(sceneImage,
                outputMat,
                perspectiveTransform,
                outputMat.size(),
                Imgproc.INTER_CUBIC);


        Mat outputWithLines = outputMat.clone();

        Fragment tmpFragment = xmlConfig.getFragment(0);
        Mat middle = outputMat.colRange(125, 180).rowRange(120, 180);

        if (tmpFragment != null) {
            middle = outputMat.colRange(tmpFragment.ymin, tmpFragment.ymax).rowRange(tmpFragment.xmin, tmpFragment.xmax);
            Imgproc.rectangle(outputWithLines, new Point(tmpFragment.ymin, tmpFragment.xmin), new Point(tmpFragment.ymax, tmpFragment.xmax), new Scalar(0, 255, 255));
        }
        Mat upper = outputMat.colRange(125, 180).rowRange(25, 40);
        tmpFragment = xmlConfig.getFragment(1);
        if (tmpFragment != null) {
            upper = outputMat.colRange(tmpFragment.ymin, tmpFragment.ymax).rowRange(tmpFragment.xmin, tmpFragment.xmax);
            Imgproc.rectangle(outputWithLines, new Point(tmpFragment.ymin, tmpFragment.xmin), new Point(tmpFragment.ymax, tmpFragment.xmax), new Scalar(0, 255, 255));
        }
        Mat bottom = outputMat.colRange(125, 180).rowRange(265, 280);
        tmpFragment = xmlConfig.getFragment(2);
        if (tmpFragment != null) {
            bottom = outputMat.colRange(tmpFragment.ymin, tmpFragment.ymax).rowRange(tmpFragment.xmin, tmpFragment.xmax);
            Imgproc.rectangle(outputWithLines, new Point(tmpFragment.ymin, tmpFragment.xmin), new Point(tmpFragment.ymax, tmpFragment.xmax), new Scalar(0, 255, 255));
        }
        Mat left = outputMat.colRange(20, 30).rowRange(120, 180);
        tmpFragment = xmlConfig.getFragment(3);
        if (tmpFragment != null) {
            left = outputMat.colRange(tmpFragment.ymin, tmpFragment.ymax).rowRange(tmpFragment.xmin, tmpFragment.xmax);
            Imgproc.rectangle(outputWithLines, new Point(tmpFragment.ymin, tmpFragment.xmin), new Point(tmpFragment.ymax, tmpFragment.xmax), new Scalar(0, 255, 255));
        }
        Mat right = outputMat.colRange(270, 280).rowRange(120, 180);
        tmpFragment = xmlConfig.getFragment(4);
        if (tmpFragment != null) {
            right = outputMat.colRange(tmpFragment.ymin, tmpFragment.ymax).rowRange(tmpFragment.xmin, tmpFragment.xmax);
            Imgproc.rectangle(outputWithLines, new Point(tmpFragment.ymin, tmpFragment.xmin), new Point(tmpFragment.ymax, tmpFragment.xmax), new Scalar(0, 255, 255));
        }

        List<Double> minMiddle = getMinRGB(middle);
        List<Double> minUpper = getMinRGB(upper);
        List<Double> minBottom = getMinRGB(bottom);
        List<Double> minLeft = getMinRGB(left);
        List<Double> minRight = getMinRGB(right);

        List<Double> maxMiddle = getMaxRGB(middle);
        List<Double> maxUpper = getMaxRGB(upper);
        List<Double> maxBottom = getMaxRGB(bottom);
        List<Double> maxLeft = getMaxRGB(left);
        List<Double> maxRight = getMaxRGB(right);

        List<Double> avgMiddle = getAvgRGB(middle);
        List<Double> avgUpper = getAvgRGB(upper);
        List<Double> avgBottom = getAvgRGB(bottom);
        List<Double> avgLeft = getAvgRGB(left);
        List<Double> avgRight = getAvgRGB(right);

        Mat tmpMat = sceneImage.clone();

        ArrayList<MatOfPoint> approxContours = new ArrayList<MatOfPoint>();
        approxContours.add(approxContour);
        Imgproc.drawContours(tmpMat, approxContours, 0, new Scalar(255, 0, 0), 1);
        
        if (writeDebugInformation) {

            scenePoints2f.convertTo(scenePoints, CvType.CV_32S);
            Point p1 = new Point(scenePoints.get(0, 0)[0], scenePoints.get(0, 1)[0]);
            Point p2 = new Point(scenePoints.get(1, 0)[0], scenePoints.get(1, 1)[0]);
            Point p3 = new Point(scenePoints.get(2, 0)[0], scenePoints.get(2, 1)[0]);
            Point p4 = new Point(scenePoints.get(3, 0)[0], scenePoints.get(3, 1)[0]);

            Imgproc.line(tmpMat, p1, p2, new Scalar(0, 0, 255));
            Imgproc.line(tmpMat, p2, p3, new Scalar(0, 0, 255));
            Imgproc.line(tmpMat, p3, p4, new Scalar(0, 0, 255));
            Imgproc.line(tmpMat, p4, p1, new Scalar(0, 0, 255));


            Imgproc.line(outputWithLines, new Point(0.0, 600.0 / 2.0), new Point(600.0, 600.0 / 2.0), new Scalar(0, 0, 255));
            Imgproc.line(outputWithLines, new Point(600.0 / 2.0, 0), new Point(600.0 / 2., 600.0), new Scalar(0, 0, 255));

            if (writeDebugImage) {
                SaveImage(tmpMat, "scene_conture");
                SaveImage(outputMat, "scene_output");
                SaveImage(outputWithLines, "outputWithLines");
                SaveImage(middle, "middle");
                SaveImage(upper, "upper");
                SaveImage(bottom, "bottom");
                SaveImage(left, "left");
                SaveImage(right, "right");
            }


            addToLog(String.format("%s - %f", "middle MIN R", minMiddle.get(0)));
            addToLog(String.format("%s - %f", "upper MIN R", minUpper.get(0)));
            addToLog(String.format("%s - %f", "bottom MIN R", minBottom.get(0)));
            addToLog(String.format("%s - %f", "left MIN R", minLeft.get(0)));
            addToLog(String.format("%s - %f", "right MIN R", minRight.get(0)));

            addToLog(String.format("%s - %f", "middle MIN G", minMiddle.get(1)));
            addToLog(String.format("%s - %f", "upper MIN G", minUpper.get(1)));
            addToLog(String.format("%s - %f", "bottom MIN G", minBottom.get(1)));
            addToLog(String.format("%s - %f", "left MIN G", minLeft.get(1)));
            addToLog(String.format("%s - %f", "right MIN G", minRight.get(1)));

            addToLog(String.format("%s - %f", "middle MIN B", minMiddle.get(2)));
            addToLog(String.format("%s - %f", "upper MIN B", minUpper.get(2)));
            addToLog(String.format("%s - %f", "bottom MIN B", minBottom.get(2)));
            addToLog(String.format("%s - %f", "left MIN B", minLeft.get(2)));
            addToLog(String.format("%s - %f", "right MIN B", minRight.get(2)));


            addToLog(String.format("%s - %f", "middle MAX R", maxMiddle.get(0)));
            addToLog(String.format("%s - %f", "upper MAX R", maxUpper.get(0)));
            addToLog(String.format("%s - %f", "bottom MAX R", maxBottom.get(0)));
            addToLog(String.format("%s - %f", "left MAX R", maxLeft.get(0)));
            addToLog(String.format("%s - %f", "right MAX R", maxRight.get(0)));

            addToLog(String.format("%s - %f", "middle MAX G", maxMiddle.get(1)));
            addToLog(String.format("%s - %f", "upper MAX G", maxUpper.get(1)));
            addToLog(String.format("%s - %f", "bottom MAX G", maxBottom.get(1)));
            addToLog(String.format("%s - %f", "left MAX G", maxLeft.get(1)));
            addToLog(String.format("%s - %f", "right MAX G", maxRight.get(1)));

            addToLog(String.format("%s - %f", "middle MAX B", maxMiddle.get(2)));
            addToLog(String.format("%s - %f", "upper MAX B", maxUpper.get(2)));
            addToLog(String.format("%s - %f", "bottom MAX B", maxBottom.get(2)));
            addToLog(String.format("%s - %f", "left MAX B", maxLeft.get(2)));
            addToLog(String.format("%s - %f", "right MAX B", maxRight.get(2)));


            addToLog(String.format("%s - %f", "middle AVG R", avgMiddle.get(0)));
            addToLog(String.format("%s - %f", "upper AVG R", avgUpper.get(0)));
            addToLog(String.format("%s - %f", "bottom AVG R", avgBottom.get(0)));
            addToLog(String.format("%s - %f", "left AVG R", avgLeft.get(0)));
            addToLog(String.format("%s - %f", "right AVG R", avgRight.get(0)));

            addToLog(String.format("%s - %f", "middle AVG G", avgMiddle.get(1)));
            addToLog(String.format("%s - %f", "upper AVG G", avgUpper.get(1)));
            addToLog(String.format("%s - %f", "bottom AVG G", avgBottom.get(1)));
            addToLog(String.format("%s - %f", "left AVG G", avgLeft.get(1)));
            addToLog(String.format("%s - %f", "right AVG G", avgRight.get(1)));

            addToLog(String.format("%s - %f", "middle AVG B", avgMiddle.get(2)));
            addToLog(String.format("%s - %f", "upper AVG B", avgUpper.get(2)));
            addToLog(String.format("%s - %f", "bottom AVG B", avgBottom.get(2)));
            addToLog(String.format("%s - %f", "left AVG B", avgLeft.get(2)));
            addToLog(String.format("%s - %f", "right AVG B", avgRight.get(2)));

            writeHistogramToFile(middle, "middle");
            writeHistogramToFile(upper, "upper");
            writeHistogramToFile(bottom, "bottom");
            writeHistogramToFile(left, "left");
            writeHistogramToFile(right, "right");
        }

        int color = 0;

        if (xmlConfig.getCompareColor().equalsIgnoreCase("green")) {
            color = 1;
        } else if (xmlConfig.getCompareColor().equalsIgnoreCase("blue")) {
            color = 2;
        } else { // red
            color = 0;
        }

        if (detectionMethode == 3) {

            Integer peakCount = xmlConfig.getPeakCount();
            RowAnalyser rowAnalyser;
            List<Integer> found3Count = new ArrayList<Integer>();


            for (int delta = 5; delta < 75; delta+=5) {
                List<Integer> intervalCount = new ArrayList<Integer>();

                rowAnalyser = getLine(outputMat, 15, 300, 300, 300, delta, "upper");
                intervalCount.add(rowAnalyser.countIntervals(1));
                if (writeDebugInformation)
                    writStringToFile("ChannelLine.txt", rowAnalyser.writeRowInformations());

                rowAnalyser = getLine(outputMat, 300, 300, 585, 300, delta, "bottom");
                intervalCount.add(rowAnalyser.countIntervals(1));
                if (writeDebugInformation)
                    writStringToFile("ChannelLine.txt", rowAnalyser.writeRowInformations());

                rowAnalyser = getLine(outputMat, 300, 15, 300, 300, delta, "left");
                intervalCount.add(rowAnalyser.countIntervals(1));
                if (writeDebugInformation)
                    writStringToFile("ChannelLine.txt", rowAnalyser.writeRowInformations());

                rowAnalyser = getLine(outputMat, 300, 300, 300, 585, delta, "right");
                intervalCount.add(rowAnalyser.countIntervals(1));
                if (writeDebugInformation)
                    writStringToFile("ChannelLine.txt", rowAnalyser.writeRowInformations());

                Collections.sort(intervalCount);
                if ((intervalCount.get(0) == intervalCount.get(2) || intervalCount.get(1) == intervalCount.get(3) ) && intervalCount.get(1) > 10 ){
                    found3Count.add(intervalCount.get(1));
                }

                if (intervalCount.get(0) == intervalCount.get(3) && intervalCount.get(0) > 10){
                    if (intervalCount.get(0) == peakCount) {
                        if (writeDebugInformation ||true)
                            addToLog(String.format("\nMARKER FOUND with %d peaks in channel %d MIN - Method 3", intervalCount.get(0), 1));
                        return 1;
                    } else {

                        if (writeDebugInformation||true)
                            addToLog(String.format("\nNO MARKER FOUND with %d peaks in channel %d MIN - Method 3", intervalCount.get(0), 1));
                        return 0;
                    }
                }
            }

            int count38 = Collections.frequency(found3Count,38);
            int count37 = Collections.frequency(found3Count,37);

            if (count37 == 0 && count38 > 0){
                if (writeDebugInformation||true)
                    addToLog(String.format("\n MARKER FOUND estimate(3x) from %d findings in %d in channel %d MIN - Method 3", count38, found3Count.size(), 1));
                return 1;
            }

        }else if (detectionMethode == 1) {
            double valueMiddle = 0;
            double valueUpper = 0;
            double valueBottom = 0;
            double valueLeft = 0;
            double valueRight = 0;
            if (xmlConfig.getCompareMethod().equalsIgnoreCase("min")) {
                valueMiddle = minMiddle.get(color);
                valueUpper = minUpper.get(color);
                valueBottom = minBottom.get(color);
                valueLeft = minLeft.get(color);
                valueRight = minRight.get(color);
            } else if (xmlConfig.getCompareMethod().equalsIgnoreCase("avg")) {
                valueMiddle = avgMiddle.get(color);
                valueUpper = avgUpper.get(color);
                valueBottom = avgBottom.get(color);
                valueLeft = avgLeft.get(color);
                valueRight = avgRight.get(color);
            } else { //max
                valueMiddle = maxMiddle.get(color);
                valueUpper = maxUpper.get(color);
                valueBottom = maxBottom.get(color);
                valueLeft = maxLeft.get(color);
                valueRight = maxRight.get(color);
            }

            int cCount = 0;

            if (xmlConfig.getCompareDirection().equalsIgnoreCase("high")) {
                if (valueMiddle > valueUpper)
                    cCount += 1;
                if (valueMiddle > valueBottom)
                    cCount += 1;
                if (valueMiddle > valueLeft)
                    cCount += 1;
                if (valueMiddle > valueRight)
                    cCount += 1;
            } else { //"low"
                if (valueMiddle < valueUpper)
                    cCount += 1;
                if (valueMiddle < valueBottom)
                    cCount += 1;
                if (valueMiddle < valueLeft)
                    cCount += 1;
                if (valueMiddle < valueRight)
                    cCount += 1;
            }

            if (cCount > 2)
                return 1;
            else
                return 0;

        } else if (detectionMethode == 2) {

            Mat middelHistB = GetHistoOpenCV.makeHist(middle, 0);
            Mat middelHistG = GetHistoOpenCV.makeHist(middle, 1);
            Mat middelHistR = GetHistoOpenCV.makeHist(middle, 2);

            Mat leftHistB = GetHistoOpenCV.makeHist(left, 0);
            Mat leftHistG = GetHistoOpenCV.makeHist(left, 1);
            Mat leftHistR = GetHistoOpenCV.makeHist(left, 2);

            Mat rightHistB = GetHistoOpenCV.makeHist(right, 0);
            Mat rightHistG = GetHistoOpenCV.makeHist(right, 1);
            Mat rightHistR = GetHistoOpenCV.makeHist(right, 2);

            Mat upperHistB = GetHistoOpenCV.makeHist(upper, 0);
            Mat upperHistG = GetHistoOpenCV.makeHist(upper, 1);
            Mat upperHistR = GetHistoOpenCV.makeHist(upper, 2);

            Mat bottomHistB = GetHistoOpenCV.makeHist(bottom, 0);
            Mat bottomHistG = GetHistoOpenCV.makeHist(bottom, 1);
            Mat bottomHistR = GetHistoOpenCV.makeHist(bottom, 2);

            int comparResult = 0;
            if (color == 0)
                comparResult = Comparator.comparePeaks(middelHistR, upperHistR, bottomHistR, leftHistR, rightHistR);
            else if (color == 1)
                comparResult = Comparator.comparePeaks(middelHistG, upperHistG, bottomHistG, leftHistG, rightHistG);
            else if (color == 2)
                comparResult = Comparator.comparePeaks(middelHistB, upperHistB, bottomHistB, leftHistB, rightHistB);

            return comparResult;
        } else if (detectionMethode == 4) {

            List<List<String>> histCols = new ArrayList<List<String>>() ;
            List<Mat> hist;

            hist = getRGBHistograms(middle);
            Mat middleHist = hist.get(2);

            if (writeDebugInformation ) {
                histCols = this.addHistCols(histCols, "middle", hist);
                Mat middle_part = getYellowPart(middle, 3);
                hist = getHSVChannels(middle_part);
                histCols = this.addHistCols(histCols, "middle_hsv", hist);
                SaveImage(getYellowPart(middle, 3), "middle_yellow.png");
                List<Double> medianHSVMiddle = getHSVMedian(getYellowPart(middle, 3));
                addToLog(String.format("\nExpectation Middle Point(%d) H %2f S %2f V %2f ", 3, medianHSVMiddle.get(0), medianHSVMiddle.get(1), medianHSVMiddle.get(2)));
            }

            hist = getRGBHistograms(left);
            Mat leftHist = hist.get(2);
            if (writeDebugInformation ) {
                Mat left_part = getYellowPart(left, 3);
                histCols = this.addHistCols(histCols, "left", hist);
                hist = getHSVChannels(left_part);
                histCols = this.addHistCols(histCols, "left_hsv", hist);
                SaveImage(getYellowPart(left, 3), "left_yellow.png");
                List<Double> medianHSVLeft = getHSVMedian(getYellowPart(left, 3));
                addToLog(String.format("\nExpectation Left Point(%d) H %2f S %2f V %2f ", 3, medianHSVLeft.get(0), medianHSVLeft.get(1), medianHSVLeft.get(2)));
            }

            hist = getRGBHistograms(right);
            Mat rightHist = hist.get(2);
            if (writeDebugInformation) {
                Mat right_part = getYellowPart(right, 3);
                histCols = this.addHistCols(histCols, "right", hist);
                hist = getHSVChannels(right_part);
                histCols = this.addHistCols(histCols, "right_hsv", hist);
                SaveImage(getYellowPart(right, 3), "right_yellow.png");
                List<Double> medianHSVRight = getHSVMedian(getYellowPart(right, 3));
                addToLog(String.format("\nExpectation Right Point(%d) H %2f S %2f V %2f ", 3, medianHSVRight.get(0), medianHSVRight.get(1), medianHSVRight.get(2)));
            }

            hist = getRGBHistograms(upper);
            Mat upperHist = hist.get(2);
            if (writeDebugInformation) {
                Mat upper_part = getYellowPart(upper, 3);
                histCols = this.addHistCols(histCols, "upper", hist);
                hist = getHSVChannels(upper);
                histCols = this.addHistCols(histCols, "upper_hsv", hist);
                SaveImage(getYellowPart(upper, 3), "upper_yellow.png");
                List<Double> medianHSVUpper = getHSVMedian(getYellowPart(upper, 3));
                addToLog(String.format("\nExpectation Upper Point(%d) H %2f S %2f V %2f ", 3, medianHSVUpper.get(0), medianHSVUpper.get(1), medianHSVUpper.get(2)));
            }

            hist = getRGBHistograms(bottom);
            Mat bottomHist = hist.get(2);
            if (writeDebugInformation) {
                Mat bottom_part = getYellowPart(bottom, 3);
                histCols = this.addHistCols(histCols, "bottom", hist);
                hist = getHSVChannels(bottom_part);
                histCols = this.addHistCols(histCols, "bottom_hsv", hist);
                SaveImage(getYellowPart(bottom, 3), "bottom_yellow.png");
                List<Double> medianHSVBottom = getHSVMedian(getYellowPart(bottom, 3));
                addToLog(String.format("\nExpectation Bottom Point(%d) H %2f S %2f V %2f ", 3, medianHSVBottom.get(0), medianHSVBottom.get(1), medianHSVBottom.get(2)));
            }

            if (writeDebugInformation)
                this.writeColsToFile("AllHists.csv", histCols);

            double expectationFullMiddle = getExpectation(middleHist, 0);
            double expectationMaxMiddle = getExpectation(middleHist, 100);
            double expectationDiffMiddle = expectationMaxMiddle - expectationFullMiddle;

            double expectationFullLeft = getExpectation(leftHist, 0);
            double expectationMaxLeft = getExpectation(leftHist, 100);
            double expectationDiffLeft = expectationMaxLeft - expectationFullLeft;

            double expectationFullRight = getExpectation(rightHist, 0);
            double expectationMaxRight = getExpectation(rightHist, 100);
            double expectationDiffRight = expectationMaxRight - expectationFullRight;

            double expectationFullUpper = getExpectation(upperHist, 0);
            double expectationMaxUpper = getExpectation(upperHist, 100);
            double expectationDiffUpper = expectationMaxUpper - expectationFullUpper;

            double expectationFullBottom = getExpectation(bottomHist, 0);
            double expectationMaxBottom = getExpectation(bottomHist, 100);
            double expectationDiffBottom = expectationMaxBottom - expectationFullBottom;

            if (writeDebugInformation || true){
                addToLog(String.format("\nExpectation Middle %d Full %f Max(%d) %f Diff %f ", 2, expectationFullMiddle, 100, expectationMaxMiddle, expectationDiffMiddle));
                addToLog(String.format("\nExpectation Left %d Full %f Max(%d) %f Diff %f ", 2, expectationFullLeft, 100, expectationMaxLeft, expectationDiffLeft));
                addToLog(String.format("\nExpectation Right %d Full %f Max(%d) %f Diff %f ", 2, expectationFullRight, 100, expectationMaxRight, expectationDiffRight));
                addToLog(String.format("\nExpectation Upper %d Full %f Max(%d) %f Diff %f ", 2, expectationFullUpper, 100, expectationMaxUpper, expectationDiffUpper));
                addToLog(String.format("\nExpectation Bottom %d Full %f Max(%d) %f Diff %f ", 2, expectationFullBottom, 100, expectationMaxBottom, expectationDiffBottom));
          
                addToLog(String.format("\n"));
            }

            int cCount = 0;
            if (xmlConfig.getCompareDirection().equalsIgnoreCase("high")) {
                if (expectationDiffMiddle >= expectationDiffLeft)
                    cCount += 1;
                if (expectationDiffMiddle >= expectationDiffRight)
                    cCount += 1;
                if (expectationDiffMiddle >= expectationDiffUpper)
                    cCount += 1;
                if (expectationDiffMiddle >= expectationDiffBottom)
                    cCount += 1;
            } else { //"low"
                if (expectationDiffMiddle <= expectationDiffLeft)
                    cCount += 1;
                if (expectationDiffMiddle <= expectationDiffRight)
                    cCount += 1;
                if (expectationDiffMiddle <= expectationDiffUpper)
                    cCount += 1;
                if (expectationDiffMiddle <= expectationDiffBottom)
                    cCount += 1;
            }

            if (cCount > 2) {
                addToLog("Marker found method 4 ");
                return 1;
            }
            else {
                addToLog("Non Marker found method 4 ");
                return 0;
            }

        } else if (detectionMethode == 5) {

            List<Double> medianHSVMiddle = getHSVMedian(getYellowPart(middle, 3));
            List<Double> medianHSVLeft = getHSVMedian(getYellowPart(left,3));
            List<Double> medianHSVRight = getHSVMedian(getYellowPart(right,3));
            List<Double> medianHSVUpper = getHSVMedian(getYellowPart(upper,3));
            List<Double> medianHSVBottom = getHSVMedian(getYellowPart(bottom,3));

            if (writeDebugInformation){
                List<List<String>> histCols = new ArrayList<List<String>>() ;
                List<Mat> hist;

                Mat middle_part = getYellowPart(middle, 3);
                hist = getRGBHistograms(middle);
                histCols = this.addHistCols(histCols, "middle", hist);
                Mat middleHist = hist.get(2);
                hist = getHSVChannels(middle_part);
                hist = getHSVChannels(middle);
                histCols = this.addHistCols(histCols, "middle_hsv", hist);

                SaveImage( getYellowPart(middle,3) , "middle_yellow.png");

                Mat left_part = getYellowPart(left,3);
                hist = getRGBHistograms(left);
                histCols = this.addHistCols(histCols, "left", hist);
                Mat leftHist = hist.get(2);
                hist = getHSVChannels(left_part);
                hist = getHSVChannels(left);
                histCols = this.addHistCols(histCols, "left_hsv", hist);

                SaveImage( getYellowPart(left,3), "left_yellow.png");

                Mat right_part = getYellowPart(right,3);
                hist = getRGBHistograms(right);
                histCols = this.addHistCols(histCols, "right", hist);
                Mat rightHist = hist.get(2);
                hist = getHSVChannels(right_part);
                hist = getHSVChannels(right);
                histCols = this.addHistCols(histCols, "right_hsv", hist);

                SaveImage( getYellowPart(right,3), "right_yellow.png");

                Mat upper_part = getYellowPart(upper,3);
                hist = getRGBHistograms(upper);
                histCols = this.addHistCols(histCols, "upper", hist);
                Mat upperHist =hist.get(2);
                hist = getHSVChannels(upper_part);
                hist = getHSVChannels(upper);
                histCols = this.addHistCols(histCols, "upper_hsv", hist);

                SaveImage( getYellowPart(upper,3), "upper_yellow.png");

                Mat bottom_part = getYellowPart(bottom,3);
                hist = getRGBHistograms(bottom);
                histCols = this.addHistCols(histCols, "bottom", hist);
                Mat bottomHist = hist.get(2);
                hist = getHSVChannels(bottom_part);
                hist = getHSVChannels(bottom);
                histCols = this.addHistCols(histCols, "bottom_hsv", hist);
                SaveImage( getYellowPart(bottom,3), "bottom_yellow.png");

                this.writeColsToFile("AllHists.csv", histCols);

                double expectationFullMiddle = getExpectation(middleHist, 0);
                double expectationMaxMiddle = getExpectation(middleHist, 100);
                double expectationDiffMiddle = expectationMaxMiddle - expectationFullMiddle;

                double expectationFullLeft = getExpectation(leftHist, 0);
                double expectationMaxLeft = getExpectation(leftHist, 100);
                double expectationDiffLeft = expectationMaxLeft - expectationFullLeft;

                double expectationFullRight = getExpectation(rightHist, 0);
                double expectationMaxRight = getExpectation(rightHist, 100);
                double expectationDiffRight = expectationMaxRight - expectationFullRight;

                double expectationFullUpper = getExpectation(upperHist, 0);
                double expectationMaxUpper = getExpectation(upperHist, 100);
                double expectationDiffUpper = expectationMaxUpper - expectationFullUpper;

                double expectationFullBottom = getExpectation(bottomHist, 0);
                double expectationMaxBottom = getExpectation(bottomHist, 100);
                double expectationDiffBottom = expectationMaxBottom - expectationFullBottom;


                addToLog(String.format("\nExpectation Middle %d Full %f Max(%d) %f Diff %f ", 2, expectationFullMiddle, 100, expectationMaxMiddle, expectationDiffMiddle));
                addToLog(String.format("\nExpectation Left %d Full %f Max(%d) %f Diff %f ", 2, expectationFullLeft, 100, expectationMaxLeft, expectationDiffLeft));
                addToLog(String.format("\nExpectation Right %d Full %f Max(%d) %f Diff %f ", 2, expectationFullRight, 100, expectationMaxRight, expectationDiffRight));
                addToLog(String.format("\nExpectation Upper %d Full %f Max(%d) %f Diff %f ", 2, expectationFullUpper, 100, expectationMaxUpper, expectationDiffUpper));
                addToLog(String.format("\nExpectation Bottom %d Full %f Max(%d) %f Diff %f ", 2, expectationFullBottom, 100, expectationMaxBottom, expectationDiffBottom));

                addToLog(String.format("\n"));

            }

            addToLog(String.format("\nExpectation Middle Point(%d) H %2f S %2f V %2f ", 3, medianHSVMiddle.get(0), medianHSVMiddle.get(1), medianHSVMiddle.get(2)));
            addToLog(String.format("\nExpectation Left Point(%d) H %2f S %2f V %2f ", 3, medianHSVLeft.get(0), medianHSVLeft.get(1), medianHSVLeft.get(2)));
            addToLog(String.format("\nExpectation Right Point(%d) H %2f S %2f V %2f ", 3, medianHSVRight.get(0), medianHSVRight.get(1), medianHSVRight.get(2)));
            addToLog(String.format("\nExpectation Upper Point(%d) H %2f S %2f V %2f ", 3, medianHSVUpper.get(0), medianHSVUpper.get(1), medianHSVUpper.get(2)));
            addToLog(String.format("\nExpectation Bottom Point(%d) H %2f S %2f V %2f ", 3, medianHSVBottom.get(0), medianHSVBottom.get(1), medianHSVBottom.get(2)));


            int cCountS = 0;
            if (xmlConfig.getCompareDirection().equalsIgnoreCase("high")) {
                if (medianHSVMiddle.get(1) >= medianHSVLeft.get(1))
                    cCountS += 1;
                if (medianHSVMiddle.get(1) >=  medianHSVRight.get(1))
                    cCountS += 1;
                if (medianHSVMiddle.get(1) >= medianHSVUpper.get(1))
                    cCountS += 1;
                if (medianHSVMiddle.get(1) >=  medianHSVBottom.get(1))
                    cCountS += 1;
            } else { //"low"
                if (medianHSVMiddle.get(1) <= medianHSVLeft.get(1))
                    cCountS += 1;
                if (medianHSVMiddle.get(1) <=  medianHSVRight.get(1))
                    cCountS += 1;
                if (medianHSVMiddle.get(1) <= medianHSVUpper.get(1))
                    cCountS += 1;
                if (medianHSVMiddle.get(1) <=  medianHSVBottom.get(1))
                    cCountS += 1;
            }

            if (cCountS == 4) {
                addToLog("Marker found method 5 ");
                return 1;
            }
            else  if (cCountS == 0) {
                addToLog("Non Marker found method 5 ");
                return 0;
            }

            int cCountH = 0;
            if (xmlConfig.getCompareDirection().equalsIgnoreCase("high")) {
                if (medianHSVMiddle.get(0) >= medianHSVLeft.get(0))
                    cCountH += 1;
                if (medianHSVMiddle.get(0) >=  medianHSVRight.get(0))
                    cCountH += 1;
                if (medianHSVMiddle.get(0) >= medianHSVUpper.get(0))
                    cCountH += 1;
                if (medianHSVMiddle.get(0) >=  medianHSVBottom.get(0))
                    cCountH += 1;
            } else { //"low"
                if (medianHSVMiddle.get(0) <= medianHSVLeft.get(0))
                    cCountH += 1;
                if (medianHSVMiddle.get(0) <=  medianHSVRight.get(0))
                    cCountH += 1;
                if (medianHSVMiddle.get(0) <= medianHSVUpper.get(0))
                    cCountH += 1;
                if (medianHSVMiddle.get(0) <=  medianHSVBottom.get(0))
                    cCountH += 1;
            }

            if (cCountH == 0) {
                addToLog("Marker found method 5 ");
                return 1;
            }
            else  if (cCountH == 4) {
                addToLog("Non Marker found method 5 ");
                return 0;
            }

            if (cCountS > 2) {
                addToLog("Marker found method 5 ");
                return 1;
            }
            else  {
                addToLog("Non Marker found method 5 ");
                return 0;
            }


        } else if (detectionMethode == -1) {

            RowAnalyser rowAnalyser, rowAnalyserNew;
            List<Integer> countsMax = new ArrayList<Integer>();
            List<Integer> countsMin = new ArrayList<Integer>();
            Integer peakCount = xmlConfig.getPeakCount();

            for (int delta = 0; delta < 20; delta++) {
                countsMax = new ArrayList<Integer>();
                countsMin = new ArrayList<Integer>();

                rowAnalyser = getLine(outputMat, 25, 300, 300, 300, delta, "upper");
                for (int j = 0; j < 3; j++) {
                    countsMax.add(rowAnalyser.getPeakCountMax(j));
                    countsMin.add(rowAnalyser.getPeakCountMin(j));
                }
                if (writeDebugInformation)
                    writStringToFile("ChannelLine.txt", rowAnalyser.writeRowInformations());

                rowAnalyser = getLine(outputMat, 300, 300, 575, 300, delta, "bottom");
                for (int j = 0; j < 3; j++) {
                    countsMax.add(rowAnalyser.getPeakCountMax(j));
                    countsMin.add(rowAnalyser.getPeakCountMin(j));
                }
                if (writeDebugInformation)
                    writStringToFile("ChannelLine.txt", rowAnalyser.writeRowInformations());

                rowAnalyser = getLine(outputMat, 300, 25, 300, 300, delta, "left");
                for (int j = 0; j < 3; j++) {
                    countsMax.add(rowAnalyser.getPeakCountMax(j));
                    countsMin.add(rowAnalyser.getPeakCountMin(j));
                }
                if (writeDebugInformation)
                    writStringToFile("ChannelLine.txt", rowAnalyser.writeRowInformations());

                rowAnalyser = getLine(outputMat, 300, 300, 300, 575, delta, "right");
                for (int j = 0; j < 3; j++) {
                    countsMax.add(rowAnalyser.getPeakCountMax(j));
                    countsMin.add(rowAnalyser.getPeakCountMin(j));
                }
                if (writeDebugInformation)
                    writStringToFile("ChannelLine.txt", rowAnalyser.writeRowInformations());

                if (writeDebugInformation) {
                    writStringToFile("ChannelLine.txt", String.format("\nDelta %d -Count Max 0: %d/ %d/ %d/ %d", delta, countsMax.get(0), countsMax.get(3), countsMax.get(6), countsMax.get(9)));
                    writStringToFile("ChannelLine.txt", String.format("\nDelta %d -Count Max 1: %d/ %d/ %d/ %d", delta, countsMax.get(1), countsMax.get(4), countsMax.get(7), countsMax.get(10)));
                    writStringToFile("ChannelLine.txt", String.format("\nDelta %d -Count Max 2: %d/ %d/ %d/ %d", delta, countsMax.get(2), countsMax.get(5), countsMax.get(8), countsMax.get(11)));
                    writStringToFile("ChannelLine.txt", String.format("\nDelta %d -Count Min 0: %d/ %d/ %d/ %d", delta, countsMin.get(0), countsMin.get(3), countsMin.get(6), countsMin.get(9)));
                    writStringToFile("ChannelLine.txt", String.format("\nDelta %d -Count Min 1: %d/ %d/ %d/ %d", delta, countsMin.get(1), countsMin.get(4), countsMin.get(7), countsMin.get(10)));
                    writStringToFile("ChannelLine.txt", String.format("\nDelta %d -Count Min 2: %d/ %d/ %d/ %d", delta, countsMin.get(2), countsMin.get(5), countsMin.get(8), countsMin.get(11)));

                    writStringToFile("Peaks.txt", String.format("\n\nDelta %d - Count Max 0: %d/ %d/ %d/ %d", delta, countsMax.get(0), countsMax.get(3), countsMax.get(6), countsMax.get(9)));
                    writStringToFile("Peaks.txt", String.format("\nDelta %d - Count Max 1: %d/ %d/ %d/ %d", delta, countsMax.get(1), countsMax.get(4), countsMax.get(7), countsMax.get(10)));
                    writStringToFile("Peaks.txt", String.format("\nDelta %d - Count Max 2: %d/ %d/ %d/ %d", delta, countsMax.get(2), countsMax.get(5), countsMax.get(8), countsMax.get(11)));
                    writStringToFile("Peaks.txt", String.format("\nDelta %d - Count Min 0: %d/ %d/ %d/ %d", delta, countsMin.get(0), countsMin.get(3), countsMin.get(6), countsMin.get(9)));
                    writStringToFile("Peaks.txt", String.format("\nDelta %d - Count Min 1: %d/ %d/ %d/ %d", delta, countsMin.get(1), countsMin.get(4), countsMin.get(7), countsMin.get(10)));
                    writStringToFile("Peaks.txt", String.format("\nDelta %d - Count Min 2: %d/ %d/ %d/ %d", delta, countsMin.get(2), countsMin.get(5), countsMin.get(8), countsMin.get(11)));
                }

                for (int channel = 0; channel < 3; channel++) {
                    if (countsMax.get(0 + channel) == countsMax.get(3 + channel) && countsMax.get(3 + channel) == countsMax.get(6 + channel) && countsMax.get(6 + channel) == countsMax.get(9 + channel) && countsMax.get(6 + channel) > 0) {
                        writStringToFile("ChannelLine.txt", "FOUND" + delta);

                        if (countsMax.get(0 + channel) == peakCount) {
                            if (writeDebugInformation)
                                addToLog(String.format("\nMARKER FOUND with %d peaks in channel %d MAX - Method 3",countsMax.get(0 + channel), channel));
                            return 1;
                        } else {
                            if (writeDebugInformation)
                                addToLog(String.format("\nNO MARKER FOUND with %d peaks in channel %d MAX - Method 3",countsMax.get(0 + channel), channel));
                            return 0;
                        }

                    }

                    if ( countsMin.get(0 + channel) == countsMin.get(3 + channel) && countsMin.get(3 + channel) == countsMin.get(6 + channel) && countsMin.get(6 + channel) == countsMin.get(9 + channel)&& countsMin.get(6 + channel) > 0) {
                        writStringToFile("ChannelLine.txt", "FOUND" + delta);

                        if (countsMin.get(0 + channel) == peakCount) {
                            if (writeDebugInformation)
                                addToLog(String.format("\nMARKER FOUND with %d peaks in channel %d MIN - Method 3",countsMax.get(0 + channel), channel));
                            return 1;
                        } else {
                            if (writeDebugInformation)
                                addToLog(String.format("\nNO MARKER FOUND with %d peaks in channel %d MIN - Method 3",countsMax.get(0 + channel), channel));
                            return 0;
                        }

                    }
                }

            /*
            Collections.sort(countsMax);

            if (Math.abs(countsMax.get(1) - countsMax.get(3)) >= 1 && Math.abs(countsMax.get(0) - countsMax.get(2)) >= 1)
                return -1;
            /*if (rowMax1 == 37 || colMax1 == 37)
                return 1;  */


            }
        }


        addToLog(String.format("NO MARKER FOUND with Method %d", detectionMethode));
        return 0;
    }

    private double getExpectation( Mat hist, int maxPixelCount) {

        double weightPixelSum = 0;
        double pixelSum = 0;
        double pixelCount = 0;

        for (int i =  hist.rows() - 1; i > -1; i--){
            pixelCount =  hist.get(i,0)[0];
            if ( maxPixelCount > 0 && pixelSum + pixelCount > maxPixelCount){
               break;
            }

            weightPixelSum +=  (i+1) * pixelCount;
            pixelSum += pixelCount;
        }

        return weightPixelSum / pixelSum;
    }
    
    private Mat getYellowPart( Mat image, int size) {

		Mat hsv = new Mat();
		Mat yellowMask = new Mat();
		Imgproc.cvtColor(image, hsv, Imgproc.COLOR_BGR2HSV);

        List<Integer> xCols = new ArrayList<Integer>();
        List<Integer> yRows = new ArrayList<Integer>();

        for (int i=0;i<14;i+=13) {
            Core.inRange(hsv, new Scalar(20-i, 50, 50), new Scalar(30+i, 255, 255), yellowMask);
            Imgproc.cvtColor(yellowMask, yellowMask, Imgproc.COLOR_GRAY2BGR);

            xCols = new ArrayList<Integer>();
            yRows = new ArrayList<Integer>();

            for (int y = 0; y < yellowMask.rows(); y++)
                for (int x = 0; x < yellowMask.cols(); x++) {
                    if (yellowMask.get(y, x)[0] > 0) {
                        xCols.add(x);
                        yRows.add(y);
                    }
                }

            if (xCols.size() > 9) {
                break;
            }
        }

		Set<Integer> hs = new HashSet<>();
		hs.addAll(xCols);
		xCols.clear();
		xCols.addAll(hs);
		
		hs.clear();
		hs.addAll(yRows);
		yRows.clear();
		yRows.addAll(hs);
		
		Collections.sort(xCols);
		Collections.sort(yRows);

        Integer xCenter = 0;
        Integer yCenter = 0;

        if (xCols.size() > 1 && yRows.size() > 1) {
            xCenter = xCols.get(xCols.size() / 2);
            yCenter = yRows.get(yRows.size() / 2);
        }else {
            size = 0;
        }

		return image.submat(yCenter - size , yCenter + size, xCenter - size, xCenter + size);
	   
    }
    
    private RowAnalyser getLine( Mat image, int xStart, int yStart, int xEnd , int yEnd, int minDelta, String addInfo) {
        int x = 1, y = 1 ,c = 1 ,dx = 1,dy = 1, currentX = 0, currentY = 0;

        if (xStart > xEnd)
            dx = -1;
        if (yStart > yEnd)
            dy = -1;

        if ( Math.abs(xStart - xEnd) > Math.abs(yStart - yEnd)) {
            dy = dy * Math.abs(yStart - yEnd) / Math.abs(xStart - xEnd);
            c = Math.abs(xStart - xEnd);
        } else {
            dx = Math.abs(xStart - xEnd) / Math.abs(yStart - yEnd);
            c = Math.abs(yStart - yEnd);
        }

        ArrayList<ArrayList<Integer> > line = new ArrayList<ArrayList<Integer>>();
        ArrayList<Integer>  point;
        int intX;
        int intY;

        for (int j = 0; j < c; j++) {
            currentX =  xStart + j * dx;
            currentY =  yStart + j * dy;

            for (int colorChannel = 0; colorChannel < 3; colorChannel++) {
                if (!(line.size() > colorChannel))
                    line.add( new ArrayList<Integer>());
                intX = Math.round(currentX);
                intY = Math.round(currentY);
                line.get(colorChannel).add(new Float(image.get(intY,intX)[colorChannel]).intValue());
            }
        }

        RowAnalyser rowAnalyser = new RowAnalyser();
        for (int colorChannel = 0; colorChannel < 3; colorChannel++) {
            rowAnalyser.addRow(line.get(colorChannel), colorChannel, minDelta, addInfo);
        }
        return rowAnalyser;
    }


    /**
     * Calculate the average of a list of color (gray)
     * @param colors - List<Double>
     * @return double - average
     */

    private double calculateAverage(List<Double> colors) {
        long sum = 0;
        for (Double color : colors) {
            sum += color;
        }
        return colors.isEmpty() ? 0 : 1.0 * sum / colors.size();
    }

    /**
     * This function extract a color channel from RGB Mat and sorted the result
     * @param image - Mat - RGB
     * @param channel - int - channel
     * @param reverse - boolean - reverse sort direction
     * @return List<Double>
     */
    private List<Double> getSortedChannelList(Mat image, int channel, boolean reverse) {
        List<Double> ret = new ArrayList<Double>();

        for (int X = 0; X < image.cols(); X++) {
            for (int Y = 0; Y < image.rows(); Y++) {
                ret.add(image.get(Y, X)[channel]);
            }
        }

        Collections.sort(ret);
        if (reverse) Collections.reverse(ret);

        return ret;
    }

    /**
     * This function calculate the min value of each channel
     * //BGR -> RGB
     * @param image - Mat - image
     * @return  List<Double> - min value of red, green and blue
     */

    private List<Double> getMinRGB(Mat image) {
        List<Double> ret = new ArrayList<Double>();

        ret.add(getSortedChannelList(image, 2, false).get(0));
        ret.add(getSortedChannelList(image, 1, false).get(0));
        ret.add(getSortedChannelList(image, 0, false).get(0));

        return ret;
    }

    /**
     * This function calculate the max value of each channel
     * //BGR -> RGB
     * @param image - Mat - image
     * @return  List<Double> - max value of red, green and blue
     */
    private List<Double> getMaxRGB(Mat image) {
        List<Double> ret = new ArrayList<Double>();

        ret.add(getSortedChannelList(image, 2, true).get(0));
        ret.add(getSortedChannelList(image, 1, true).get(0));
        ret.add(getSortedChannelList(image, 0, true).get(0));

        return ret;
    }

    /**
     * This function calculate the average value of each channel
     * //BGR -> RGB
     * @param image - Mat - image
     * @return  List<Double> - average value of red, green and blue
     */
    private List<Double> getAvgRGB(Mat image) {
        List<Double> ret = new ArrayList<Double>();
        ret.add(calculateAverage(getSortedChannelList(image, 2, false)));
        ret.add(calculateAverage(getSortedChannelList(image, 1, false)));
        ret.add(calculateAverage(getSortedChannelList(image, 0, false)));

        return ret;
    }

    /**
     * Extract the conture of the template
     * @param templateImage - Mat - scene image (BGR)
     * @return MatOfPoint2f -  approximated conture of the template object
     */
    private MatOfPoint getTemplateConture(Mat templateImage) {
        Mat imgSource = templateImage.clone();
        //Mat imgSource = new Mat();
        //Size sz = new Size(600,600);
        //Imgproc.resize( templateImage, imgSource, sz );

        Imgproc.cvtColor(imgSource, imgSource, Imgproc.COLOR_BGR2GRAY);
        Imgproc.Canny(imgSource, imgSource, 50, 50);

        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        List<MatOfPoint> approxContours = new ArrayList<MatOfPoint>();

        Imgproc.findContours(imgSource, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
        MatOfPoint2f tmpContours2f = new MatOfPoint2f();
        MatOfPoint2f tmpApprox2f = new MatOfPoint2f();
        contours.get(0).convertTo(tmpContours2f, CvType.CV_32FC2);

        MatOfPoint retApprox = new MatOfPoint();
        Imgproc.approxPolyDP(tmpContours2f, tmpApprox2f, 3, true);
        tmpApprox2f.convertTo(retApprox, CvType.CV_32S);

        templatePointsInt = new Mat();
        templatePoints = convexHullRect(retApprox) ;
        // templatePoints = new Mat();
        // Imgproc.boxPoints(Imgproc.minAreaRect(tmpApprox2f), templatePoints);
        templatePoints.convertTo(templatePointsInt, CvType.CV_32S);
        Point p1 = new Point(templatePointsInt.get(0, 0)[0], templatePointsInt.get(0, 1)[0]);
        Point p2 = new Point(templatePointsInt.get(1, 0)[0], templatePointsInt.get(1, 1)[0]);
        Point p3 = new Point(templatePointsInt.get(2, 0)[0], templatePointsInt.get(2, 1)[0]);
        Point p4 = new Point(templatePointsInt.get(3, 0)[0], templatePointsInt.get(3, 1)[0]);
        Imgproc.line(templateImage, p1, p2, new Scalar(0, 0, 255));
        Imgproc.line(templateImage, p2, p3, new Scalar(0, 0, 255));
        Imgproc.line(templateImage, p3, p4, new Scalar(0, 0, 255));
        Imgproc.line(templateImage, p4, p1, new Scalar(0, 0, 255));

        if(writeDebugImage)
            SaveImage(templateImage, "templateImage");

        return retApprox;
    }

    /*

    public void getBox(String subName) {


    }

    public static Point intersectLines(Point l, Point m)throws IllegalArgumentException
    {
        // Wegen der Lesbarkeit
        double x1 = l.getX1();
        double x2 = l.getX2();
        double x3 = m.getX1();
        double x4 = m.getX2();
        double y1 = l.getY1();
        double y2 = l.getY2();
        double y3 = m.getY1();
        double y4 = m.getY2();

        // Zaehler
        double zx = (x1 * y2 - y1 * x2)*(x3-x4) - (x1 - x2) * (x3 * y4 - y3 * x4);
        double zy = (x1 * y2 - y1 * x2)*(y3-y4) - (y1 - y2) * (x3 * y4 - y3 * x4);

        // Nenner
        double n = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);

        // Koordinaten des Schnittpunktes
        double x = zx/n;
        double y = zy/n;

        // Vielleicht ist bei der Division durch n etwas schief gelaufen
        if (Double.isNaN(x)& Double.isNaN(y))
        {
            throw new IllegalArgumentException("Schnittpunkt nicht eindeutig.");
        }
        // Test ob der Schnittpunkt auf den angebenen Strecken liegt oder außerhalb.
        if ((x - x1) / (x2 - x1) > 1 || (x - x3) / (x4 - x3) > 1 || (y - y1) / (y2 - y1) > 1 || (y - y3) / (y4 - y3) > 1 )
        {
            throw new IllegalArgumentException("Schnittpunkt liegt außerhalb.");
        }
        return new Point2D.Double(x,y);
    }
    */

    public File getLocalStorageDir(String subName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS), subName);
        if (!file.mkdirs()) {
            //logMessage( "Directory not created");
        }

        MediaScannerConnection.scanFile(context, new String[]{file.getAbsolutePath().toString()}, null, null);
        return file;
    }

    /**
     * write the image with the given filename
     * @param mat - Mat -image
     * @param filename - String - filename
     */
    public void SaveImage(Mat mat, String filename) {

        //File file = new File(path, filename + ".png");
        File file = new File(logPath, filename + ".png");
        filename = file.toString();
        Imgcodecs.imwrite(filename, mat);
    }

    public void SaveJpeg(Mat mat, String filename) {

        //File file = new File(path, filename + ".png");
        File file = new File(logPath, filename + ".jpg");
        filename = file.toString();
        Imgcodecs.imwrite(filename, mat);
    }

    /**
     *  write a line to the logfile
     * @param text - string
     * @return
     */
    public boolean addToLog(String text) {

        File histFile = new File(logPath, "Log.txt");
        try {
            histFile.createNewFile();
        } catch (IOException exc) {
            // TODO Auto-generated catch block
            exc.printStackTrace();
        }

        try {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(histFile, true));
            buf.append(text);
            buf.newLine();

            buf.close();
        } catch (IOException exc) {
            // TODO Auto-generated catch block
            exc.printStackTrace();
        }

        return false;
    }


    public boolean writeLine(String filename, String info , Mat image, int row) {

        File lineFile = new File(logPath, filename);
        try {
            lineFile.createNewFile();
        } catch (IOException exc) {
            // TODO Auto-generated catch block
            exc.printStackTrace();
        }

        try {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(lineFile, true));

            for (int i = 0; i < 3; i++) {
                buf.append(String.format(info + " C%d L%d: ", i, row));
                for (int j = 0; j < image.cols(); j++) {
                    buf.append(String.format("%3.0f,", image.get(row, j)[i]));
                }
                buf.newLine();
            }
            buf.close();
        } catch (IOException exc) {
            // TODO Auto-generated catch block
            exc.printStackTrace();
        }

        return false;
    }

    public boolean writStringToFile(String filename, String text) {

        File lineFile = new File(logPath, filename);
        try {
            lineFile.createNewFile();
        } catch (IOException exc) {
            // TODO Auto-generated catch block
            exc.printStackTrace();
        }

        try {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(lineFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        } catch (IOException exc) {
            // TODO Auto-generated catch block
            exc.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     *  write a file with histogram information
     * @param filename String
     * @param hist - Mat - histogram
     * @return
     */
    public boolean writeHistogram(String filename, Mat hist) {

        File histFile = new File(logPath, filename);
        try {
            histFile.createNewFile();
        } catch (IOException exc) {
            // TODO Auto-generated catch block
            exc.printStackTrace();
        }

        try {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(histFile, true));
            for (int j = 0; j < hist.rows(); j++) {
                buf.append(String.format("%d - %f", j, hist.get(j, 0)[0]));

                buf.newLine();
            }
            buf.close();
        } catch (IOException exc) {
            // TODO Auto-generated catch block
            exc.printStackTrace();
        }

        return false;
    }

    /**
     * calculate and write a histogram for each channel to local storage
     * @param fragmentMat - Mat - image
     * @param filename - String
     * @return
     */
    public boolean writeHistogramToFile(Mat fragmentMat, String filename) {

        List<Mat> bgrHistograms = getRGBHistograms(fragmentMat) ;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        String currentDateandTime = sdf.format(new Date());

        writeHistogram(filename + "_HISTOGRAM_B_" + currentDateandTime + ".txt", bgrHistograms.get(2));
        writeHistogram(filename + "_HISTOGRAM_G_" + currentDateandTime + ".txt", bgrHistograms.get(1));
        writeHistogram(filename + "_HISTOGRAM_R_" + currentDateandTime + ".txt", bgrHistograms.get(0));

        return true;
    }

    public List<Mat> getRGBHistograms(Mat fragmentMat) {

        List<Mat> ret = new ArrayList<Mat>();

        List<Mat> bgrFragmentMat = new ArrayList<Mat>();
        Core.split(fragmentMat, bgrFragmentMat);

        Mat b_hist = new Mat();
        Mat g_hist = new Mat();
        Mat r_hist = new Mat();

        MatOfFloat ranges = new MatOfFloat(0, 256);
        MatOfInt histSizeMat = new MatOfInt(255);
        MatOfInt channels = new MatOfInt(0);

        Imgproc.calcHist(bgrFragmentMat.subList(0, 1), channels, new Mat(), b_hist, histSizeMat, ranges);
        Imgproc.calcHist(bgrFragmentMat.subList(1, 2), channels, new Mat(), g_hist, histSizeMat, ranges);
        Imgproc.calcHist(bgrFragmentMat.subList(2, 3), channels, new Mat(), r_hist, histSizeMat, ranges);

        ret.add(b_hist);
        ret.add(g_hist);
        ret.add(r_hist);
        return ret;

    }

    public List<Double> getHSVMedian(Mat fragmentMat) {
    	List<Double> ret = new ArrayList<Double>();
    	List<Mat> histHSV =  getHSVChannels(fragmentMat);
    	Double sum = 0.0, sumCount = 0.0; 
    	for (int i = 0; i< histHSV.size();i++){
    		sum = 0.0;
    		sumCount = 0.0; 
    		for (int j = 0; j< histHSV.get(i).rows();j++){
    			
    			if (histHSV.get(i).get(j, 0)[0] > 0.0){
	    			sum += (j+1) * histHSV.get(i).get(j, 0)[0];
	    			sumCount +=  histHSV.get(i).get(j, 0)[0];
    			}
    		}
    		
    		ret.add(sum/sumCount);
    	}
    	
    	return ret;
    	
    }
    public List<Mat> getHSVChannels(Mat fragmentMat) {

        List<Mat> ret = new ArrayList<Mat>();
        Mat fragmentHSVMat = new Mat();
        Mat fragmentYellowMask = new Mat();

        Imgproc.cvtColor(fragmentMat, fragmentHSVMat, Imgproc.COLOR_BGR2HSV);
       
        Mat histFragmentHMat = new Mat();
        Mat histFragmentSMat = new Mat();
        Mat histFragmentVMat = new Mat();

        ArrayList<Mat> histImages=new ArrayList<Mat>();
               
        histImages.clear();
        histImages=new ArrayList<Mat>();
        histImages.add(fragmentHSVMat);
        Imgproc.calcHist(histImages,
        		new MatOfInt(0),
                new Mat(),
                histFragmentHMat,
                new MatOfInt( 180),
                new MatOfFloat(0f,180f ),
                false);        
        ret.add(histFragmentHMat);
        
        histImages.clear();
        histImages.add(fragmentHSVMat);
        Imgproc.calcHist(histImages,
                new MatOfInt(1),
                new Mat(),
                histFragmentSMat,
                new MatOfInt(256),
                new MatOfFloat(0f, 256f),
                false);        
        ret.add(histFragmentSMat);
        
        histImages.clear();
        histImages.add(fragmentHSVMat);
        Imgproc.calcHist(histImages,
                new MatOfInt(2),
                new Mat(),
                histFragmentVMat,
                new MatOfInt(256),
                new MatOfFloat(0f, 256f),
                false);     
        
        ret.add(histFragmentVMat);
         
        
        return ret;

    }


    public  List<List<String>> addHistCols(List<List<String>> cols,String name, List<Mat> hist) {
        List<String> col;

        for(int i = 0; i < hist.size();i++) {
            col = new ArrayList<>();
            col.add(String.format("%s(C%d)", name, i));

            for (int j = 0; j < hist.get(i).rows(); j++) {
                col.add(String.format("%d", (int)hist.get(i).get(j, 0)[0]));
            }
            cols.add(col);
        }

        return cols;
    }

    public  boolean writeColsToFile(String filename, List<List<String>> cols) {

        File histFile = new File(logPath,filename);
        try {
            histFile.createNewFile();
        } catch (IOException exc) {
            // TODO Auto-generated catch block
            exc.printStackTrace();
        }

        try {
            int j = 0;
            boolean valuesWriten = true;
            BufferedWriter buf = new BufferedWriter(new FileWriter(histFile, true));

            while (valuesWriten) {

                valuesWriten = false;
                for (int i = 0; i < cols.size(); i++) {


                    if (cols.get(i).size() > j) {
                        buf.append(cols.get(i).get(j));
                        valuesWriten = true;
                    }
                    buf.append(",");

                }
                j++;
                buf.newLine();
            }

            buf.flush();
            buf.close();
        } catch (IOException exc) {
            // TODO Auto-generated catch block
            exc.printStackTrace();
        }

        return false;
    }

}