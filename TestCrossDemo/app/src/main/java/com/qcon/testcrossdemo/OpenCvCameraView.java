package com.qcon.testcrossdemo;

/**
 * Created by ck on 21.08.15.
 */

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.opencv.android.JavaCameraView;

import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.util.AttributeSet;
import android.util.Log;

public class OpenCvCameraView  extends JavaCameraView implements PictureCallback {

    private static final String TAG = "TestMoneyDemo::Camera";
    private String mPictureFileName;
    protected MainActivity cbCamera;
    public OpenCvCameraView(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public List<String> getFocusList() {
        return mCamera.getParameters().getSupportedFocusModes();
    }

    public String getFocusMethod() {
        return mCamera.getParameters().getFocusMode();
    }

    public void setFocusMethod(String focus) {
        Camera.Parameters params = mCamera.getParameters();
        params.setFocusMode(focus);
        mCamera.setParameters(params);
    }

    public List<String> getFlashList() {
        return mCamera.getParameters().getSupportedFlashModes();
    }

    public List<String> getEffectList() {
        return mCamera.getParameters().getSupportedColorEffects();
    }

    public String getFlashMethod() {
        return mCamera.getParameters().getFlashMode();
    }

    public void setFlashMethod(String flash) {
        Camera.Parameters params = mCamera.getParameters();
        params.setFlashMode(flash);
        mCamera.setParameters(params);
    }

    public boolean isEffectSupported() {
        return (mCamera.getParameters().getColorEffect() != null);
    }

    public String getEffect() {
        return mCamera.getParameters().getColorEffect();
    }

    public void setEffect(String effect) {
        Camera.Parameters params = mCamera.getParameters();
        params.setColorEffect(effect);
        mCamera.setParameters(params);
    }

    public List<Integer> getSupportedPictureFormats() {
        return mCamera.getParameters().getSupportedPictureFormats();
    }

    public List<Size> getPreviewResolutionList() {
        return mCamera.getParameters().getSupportedPreviewSizes();
    }

    public Size getPreviewResolution() {
        return mCamera.getParameters().getPreviewSize();
    }

    public void setPreviewResolution(Size resolution) {
        disconnectCamera();
        mMaxHeight = resolution.height;
        mMaxWidth = resolution.width;
        connectCamera(getWidth(), getHeight());
    }

    public List<Size> getResolutionList() {
        return mCamera.getParameters().getSupportedPictureSizes();
    }

    public void setResolution(Size resolution) {
        Camera.Parameters params = mCamera.getParameters();
        params.setPictureSize(resolution.width, resolution.height);
        mCamera.setParameters(params);
    }

    public void takePicture(final String fileName) {
        Log.i(TAG, "Taking picture");
        this.mPictureFileName = fileName;
        // Postview and jpeg are sent in the same buffers if the queue is not empty when performing a capture.
        // Clear up buffers to avoid mCamera.takePicture to be stuck because of a memory issue
        mCamera.setPreviewCallback(null);

        // PictureCallback is implemented by the current class
        //mCamera.takePicture(null,  this, null);
        mCamera.takePicture(null, null, this);
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        Log.i(TAG, "Saving a bitmap to file");
        // The camera preview was automatically stopped. Start it again.
        mCamera.startPreview();
        mCamera.setPreviewCallback(this);
    }

    public void setFocus(final Rect tfocusRect ,Camera.AutoFocusCallback cb ) {

        try {
            List<Camera.Area> focusList = new ArrayList<Camera.Area>();
            Camera.Area focusArea = new Camera.Area(tfocusRect, 1000);
            focusList.add(focusArea);

            Camera.Parameters param = mCamera.getParameters();
            param.setFocusAreas(focusList);
            param.setMeteringAreas(focusList);
            mCamera.setParameters(param);
            mCamera.autoFocus(cb);

        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "Unable to autofocus");
        }
    }

    public void cancelAutoFocus(){
        mCamera.cancelAutoFocus();
    }
}