package com.qcon.testcrossdemo;

import android.content.Context;
import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 *
 */
public class XMLConfig {

    Context context;
    ArrayList<Fragment> fragments = new ArrayList<>();
    File path = null;
    String compareMinMaxAvg = "";
    String compareHighLow = "";
    String compareRedGreenBlue = "";
    int peakCount = 38;

    public XMLConfig(Context context, File path) {
        this.context = context;
        this.path = path;
        setConfig();
    }

    /**
     * read a xml file from local storage, if the file not exist a file with default value created
     * @return Document - DOM object
     */
    public Document getXMLDocFile() {
        Document document = null;

        File xmlFile = new File(path, "Extraction.xml");
        if (!xmlFile.exists()) {
            try {
                xmlFile.createNewFile();
                BufferedWriter buf = new BufferedWriter(new FileWriter(xmlFile));
                //buf.append("<QdetectorConfig><fragment xmin='6' ymin='16' xmax='138' ymax='106' hmin='10' hmax='20' filename='extractImage1'></fragment><fragment xmin='10' ymin='440' xmax='170' ymax='530' hmin='10' hmax='20' filename='extractImage2'></fragment><fragment xmin='880' ymin='240' xmax='970' ymax='300' hmin='10' hmax='20' filename='extractImage3'></fragment></QdetectorConfig>");
                //buf.append("<CrossDemoConfig><fragments><fragment xmin='120' ymin='125' xmax='180' ymax='180' name='middle'></fragment><fragment xmin='25' ymin='125' xmax='40' ymax='180' name='upper'></fragment><fragment xmin='265' ymin='125' xmax='280' ymax='180'  name='bottom'></fragment><fragment xmin='120' ymin='20' xmax='180' ymax='30' name='left'></fragment><fragment xmin='120' ymin='270' xmax='180' ymax='280'  name='right'></fragment></fragments><parameter compareMinMaxAvg='max' compareHighLow='high' compareRedGreenBlue='red' ></parameter></CrossDemoConfig>");
                //buf.append("<CrossDemoConfig><fragments><fragment xmin='130' ymin='130' xmax='170' ymax='170' name='middle'></fragment><fragment xmin='30' ymin='130' xmax='70' ymax='170' name='upper'></fragment><fragment xmin='230' ymin='130' xmax='270' ymax='170'  name='bottom'></fragment><fragment xmin='130' ymin='30' xmax='170' ymax='70' name='left'></fragment><fragment xmin='130' ymin='230' xmax='170' ymax='270'  name='right'></fragment></fragments><parameter compareMinMaxAvg='max' compareHighLow='high' compareRedGreenBlue='blue' ></parameter></CrossDemoConfig>");
                //buf.append("<CrossDemoConfig><fragments><fragment xmin='150' ymin='150' xmax='160' ymax='160' name='middle'></fragment><fragment xmin='50' ymin='150' xmax='60' ymax='160' name='upper'></fragment><fragment xmin='250' ymin='150' xmax='260' ymax='160'  name='bottom'></fragment><fragment xmin='150' ymin='50' xmax='160' ymax='60' name='left'></fragment><fragment xmin='150' ymin='250' xmax='160' ymax='260'  name='right'></fragment></fragments><parameter compareMinMaxAvg='max' compareHighLow='high' compareRedGreenBlue='blue'  peakCount='38' ></parameter></CrossDemoConfig>");
                //buf.append("<CrossDemoConfig><fragments><fragment xmin='286' ymin='294' xmax='307' ymax='314' name='middle'></fragment><fragment xmin='100' ymin='300' xmax='120' ymax='320' name='upper'></fragment><fragment xmin='500' ymin='300' xmax='520' ymax='320'  name='bottom'></fragment><fragment xmin='300' ymin='100' xmax='320' ymax='120' name='left'></fragment><fragment xmin='300' ymin='500' xmax='320' ymax='520'  name='right'></fragment></fragments><parameter compareMinMaxAvg='max' compareHighLow='high' compareRedGreenBlue='blue'  peakCount='38' ></parameter></CrossDemoConfig>");
                buf.append("<CrossDemoConfig><fragments><fragment xmin='285' ymin='285' xmax='315' ymax='315' name='middle'></fragment><fragment xmin='85' ymin='285' xmax='115' ymax='315' name='upper'></fragment><fragment xmin='485' ymin='285' xmax='515' ymax='315'  name='bottom'></fragment><fragment xmin='285' ymin='85' xmax='315' ymax='115' name='left'></fragment><fragment xmin='285' ymin='485' xmax='315' ymax='515'  name='right'></fragment></fragments><parameter compareMinMaxAvg='max' compareHighLow='high' compareRedGreenBlue='blue'  peakCount='38' ></parameter></CrossDemoConfig>");

                buf.newLine();
                buf.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            InputStream inputStream = new FileInputStream(xmlFile);
            document = getDocument(inputStream);
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return document;
    }

    /**
     * get framgment
     * @param i int
     * @return Fragment
     */
    public Fragment getFragment(int i) {
        if (fragments.size() > i)
            return fragments.get(i);
        return null;
    }


    public Integer getPeakCount() {

        return peakCount;
    }

    public String getCompareMethod() {

        return compareMinMaxAvg;
    }

    public String getCompareDirection() {

        return compareHighLow;
    }

    public String getCompareColor() {

        return compareRedGreenBlue;
    }

    /**
     * parse input stream and create a DOM object
     * @param inputStream
     * @return
     */
    public Document getDocument(InputStream inputStream) {
        Document document = null;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = factory.newDocumentBuilder();
            InputSource inputSource = new InputSource(inputStream);
            document = db.parse(inputSource);
        } catch (ParserConfigurationException e) {
            Log.e("Error ParserConfigExc: ", e.getMessage());
            return null;
        } catch (SAXException e) {
            Log.e("Error SAXException: ", e.getMessage());
            return null;
        } catch (IOException e) {
            Log.e("Error IOException: ", e.getMessage());
            return null;
        }
        return document;
    }

    /**
     * read xml values and set class member
     * @return
     */
    public boolean setConfig() {

        Document doc = getXMLDocFile();
        NodeList n1 = doc.getElementsByTagName("fragment");
        for (int i = 0; i < n1.getLength(); i++) {

            Element e = (Element) n1.item(i);
            Fragment fragment = new Fragment();

            try {
                fragment.xmin = Integer.parseInt(e.getAttribute("xmin"));
                fragment.xmax = Integer.parseInt(e.getAttribute("xmax"));
                fragment.ymax = Integer.parseInt(e.getAttribute("ymax"));
                fragment.ymin = Integer.parseInt(e.getAttribute("ymin"));
                fragment.name = e.getAttribute("name");
            } catch (NumberFormatException exc) {
                Log.e("Error: ", exc.getMessage());
            }


            this.fragments.add(fragment);
        }

        n1 = doc.getElementsByTagName("parameter");
        for (int i = 0; i < n1.getLength(); i++) {

            Element e = (Element) n1.item(i);
            try {
                compareMinMaxAvg = e.getAttribute("compareMinMaxAvg");
            } catch (NumberFormatException exc) {
                Log.e("Error: ", exc.getMessage());
            }
            try {
                compareHighLow = e.getAttribute("compareHighLow");
            } catch (NumberFormatException exc) {
                Log.e("Error: ", exc.getMessage());
            }
            try {
                compareRedGreenBlue = e.getAttribute("compareRedGreenBlue");
            } catch (NumberFormatException exc) {
                Log.e("Error: ", exc.getMessage());
            }
            try {
                int tmp = Integer.parseInt(e.getAttribute("peakCount"));
                peakCount = tmp;
            } catch (NumberFormatException exc) {
                Log.e("Error: ", exc.getMessage());
            }

        }

        return true;
    }


}
