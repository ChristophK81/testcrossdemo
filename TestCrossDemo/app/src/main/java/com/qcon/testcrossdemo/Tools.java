package com.qcon.testcrossdemo;

import org.opencv.core.Mat;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by ck on 10.09.15.
 */
public class Tools {

    private static File logPath = null;

    /**
     *  write a line to the logfile
     * @param text - string
     * @return
     */

    public static boolean addToLog(String text) {

        File histFile = new File(logPath, "Log.txt");
        try {
            histFile.createNewFile();
        } catch (IOException exc) {
            // TODO Auto-generated catch block
            exc.printStackTrace();
        }

        try {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(histFile, true));
            buf.append(text);
            buf.newLine();
            buf.flush();
            buf.close();
        } catch (IOException exc) {
            // TODO Auto-generated catch block
            exc.printStackTrace();
        }

        return false;
    }

    public  static List<List<String>> addHistCols(List<List<String>> cols,String name, List<Mat> hist) {
        List<String> col = new ArrayList<>();

        for(int i = 0; i < hist.size();i++) {
            col.add(String.format("%s(C%d)", name, i));

            for (int j = 0; j < hist.get(i).rows(); j++) {
                col.add(String.format("%d", hist.get(i).get(j, 0)[0]));
            }
        }

        cols.add(col);
        return cols;
    }

    public static boolean writeColsToFile(String filename, List<List<String>> cols) {

        File histFile = new File(logPath,filename);
        try {
            histFile.createNewFile();
        } catch (IOException exc) {
            // TODO Auto-generated catch block
            exc.printStackTrace();
        }

        try {
            int j = 0;
            boolean valuesWriten = true;
            BufferedWriter buf = new BufferedWriter(new FileWriter(histFile, true));

            while (valuesWriten) {

                valuesWriten = false;
                for (int i = 0; i < cols.size(); i++) {


                    if (cols.get(i).size() > j) {
                        buf.append(cols.get(i).get(j));
                        valuesWriten = true;
                    }
                    buf.append(",");

                }
                buf.newLine();
            }

            buf.flush();
            buf.close();
        } catch (IOException exc) {
            // TODO Auto-generated catch block
            exc.printStackTrace();
        }

        return false;
    }


}
