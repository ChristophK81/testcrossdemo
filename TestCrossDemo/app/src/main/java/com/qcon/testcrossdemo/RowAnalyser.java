package com.qcon.testcrossdemo;

import org.opencv.core.Mat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ck on 10.09.15.
 */
public class RowAnalyser {
    List<RowInformation> rows;

    public RowAnalyser() {
        rows = new ArrayList<RowInformation>();
    }

    public boolean addRow(List<Integer> line, int channel, int minDelta, String addInfo){
        rows.add(new RowInformation(line, channel,minDelta, addInfo));
        return true;
    }

    public String writeRowInformations(){
        String out = new String();
        for (int j = 0; j <  rows.size(); j++) {
            out = out.concat(rows.get(j).writeRowInformation());
        }
        out = out.concat("\n");
        return out;
    }

    public int countIntervals(int channel){
    	 RowInformation row = rows.get(channel);
         return row.countIntervals();
    }

    public int getPeakCountMin(int channel){
        RowInformation row = rows.get(channel);
        return row.countPeakMin();
    }

    public int getPeakCountMax(int channel){
        RowInformation row = rows.get(channel);
        return row.countPeakMax();
    }

}
