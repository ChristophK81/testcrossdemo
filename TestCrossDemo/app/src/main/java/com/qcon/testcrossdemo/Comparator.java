package com.qcon.testcrossdemo;

import android.util.Log;

import org.opencv.core.Mat;

import java.util.ArrayList;

/**
 * Created by ElieB on 11/07/15.
 */
public class Comparator {


    public static int comparePeaks(Mat middle, Mat upper, Mat bottom, Mat left, Mat right) {

        ArrayList<Group> middleHistGroups = GetHistoOpenCV.findAllMax(middle);
        ArrayList<Group> upperHistGroups  = GetHistoOpenCV.findAllMax(upper);
        ArrayList<Group> bottomHistGroups = GetHistoOpenCV.findAllMax(bottom);
        ArrayList<Group> leftHistGroups   = GetHistoOpenCV.findAllMax(left);
        ArrayList<Group> rightHistGroups  = GetHistoOpenCV.findAllMax(right);

        int defValueMU = 0 ;
        int defValueMB = 0 ;
        int defValueML = 0;
        int defValueMR = 0;

        Log.d("Comparator","the size of the middleHistGroups for red histogram is:  " + middleHistGroups.size());
        Tools.addToLog("middleHistGroups:  " + middleHistGroups.size());
        Log.d("Comparator", "the size of the upperHistGroups for red histogram is:   " + upperHistGroups.size());
        Tools.addToLog("upperHistGroups:  " + upperHistGroups.size());
        Log.d("Comparator", "the size of the bottomHistGroups for red histogram is:   " + bottomHistGroups.size());
        Tools.addToLog("bottomHistGroups:  " + bottomHistGroups.size());
        Log.d("Comparator", "the size of the leftHistGroups for red histogram is:  " + leftHistGroups.size());
        Tools.addToLog("leftHistGroups:  " + leftHistGroups.size());
        Log.d("Comparator", "the size of the rightHistGroups for red histogram is:  " + rightHistGroups.size());
        Tools.addToLog("rightHistGroups:  " + rightHistGroups.size());

        for (int n = 0; n < middleHistGroups.size(); n++) {
            Log.d("The middel R Idx " + middleHistGroups.get(n).getIdx(), "has the value:  " + middleHistGroups.get(n).getValue());
            Tools.addToLog("The middel Idx " + middleHistGroups.get(n).getIdx() +  " has the value:  " + middleHistGroups.get(n).getValue());
        }
        for (int n = 0; n < upperHistGroups.size(); n++) {
            Log.d("The upper R Idx " + upperHistGroups.get(n).getIdx(), "has the value:  " + upperHistGroups.get(n).getValue());
            Tools.addToLog("The upper Idx " + upperHistGroups.get(n).getIdx() + " has the value:  " + upperHistGroups.get(n).getValue());
        }
        for (int n = 0; n < bottomHistGroups.size(); n++) {
            Log.d("The bottom R Idx " + bottomHistGroups.get(n).getIdx(), "has the value:  " + bottomHistGroups.get(n).getValue());
            Tools.addToLog("The bottom Idx " + bottomHistGroups.get(n).getIdx() + " has the value:  " + bottomHistGroups.get(n).getValue());
        }
        for (int n = 0; n < rightHistGroups.size(); n++) {
            Log.d("The right R Idx " + rightHistGroups.get(n).getIdx(), "has the value:  " + rightHistGroups.get(n).getValue());
            Tools.addToLog("The right Idx " + rightHistGroups.get(n).getIdx() + " has the value:  " + rightHistGroups.get(n).getValue());
        }
        for (int n = 0; n < leftHistGroups.size(); n++) {
            Log.d("The left R Idx " + leftHistGroups.get(n).getIdx(), "has the value:  " + leftHistGroups.get(n).getValue());
            Tools.addToLog("The left Idx " + leftHistGroups.get(n).getIdx() + " has the value:  " + leftHistGroups.get(n).getValue());
        }
        int middleHistValuesSum = 0;
        int upperHistValuesSum  = 0;
        int leftHistValuesSum   = 0;
        int reightHistValuesSum = 0;
        int bottomHistValuesSum = 0;

        for (int h = 0; h < middleHistGroups.size(); h++){
            middleHistValuesSum = middleHistValuesSum + middleHistGroups.get(h).getValue();
        }
        for (int h = 0; h < upperHistGroups.size(); h++){
            upperHistValuesSum = upperHistValuesSum + upperHistGroups.get(h).getValue();
        }
        for (int h = 0; h < leftHistGroups.size(); h++){
            leftHistValuesSum = leftHistValuesSum + leftHistGroups.get(h).getValue();
        }
        for (int h = 0; h < rightHistGroups.size(); h++){
            reightHistValuesSum = reightHistValuesSum + rightHistGroups.get(h).getValue();
        }
        for (int h = 0; h < bottomHistGroups.size(); h++){
            bottomHistValuesSum = bottomHistValuesSum + bottomHistGroups.get(h).getValue();
        }

        Log.d("the  sum middel is: " + middleHistValuesSum, " ");

        Log.d("the  sum upper  is: " + upperHistValuesSum, " ");

        Log.d("the  sum left   is: " + leftHistValuesSum, " ");

        Log.d("the  sum right  is: " + reightHistValuesSum, " ");

        Log.d("the  sum bottom is: " + bottomHistValuesSum, " ");

        if ((middleHistValuesSum > upperHistValuesSum) && (middleHistValuesSum > leftHistValuesSum) && (middleHistValuesSum > reightHistValuesSum) && (middleHistValuesSum > bottomHistValuesSum)){
            return 1;
        }else return 0;

      /*  if (((middleHistValuesSum > upperHistValuesSum) && (middleHistValuesSum > leftHistValuesSum) && (middleHistValuesSum > reightHistValuesSum)) || ((middleHistValuesSum > bottomHistValuesSum) && (middleHistValuesSum > leftHistValuesSum) && (middleHistValuesSum > reightHistValuesSum)) || ((middleHistValuesSum > upperHistValuesSum) && (middleHistValuesSum > bottomHistValuesSum) && (middleHistValuesSum > reightHistValuesSum)) || ((middleHistValuesSum > upperHistValuesSum) && (middleHistValuesSum > leftHistValuesSum) && (middleHistValuesSum > bottomHistValuesSum))){
            return 1;
        }else return 0;*/
        //the meddel with the upper side of the cross
       /* if (middleHistGroups.size() > upperHistGroups.size()){

        defValueMU = 1;

            /*for (int y = 0; y < middleHistGroups.size(); y++){
                if (middleHistGroups.get(y).getIdx() == upperHistGroups.get(y).getIdx()){
                    if (middleHistGroups.get(y).getValue() > upperHistGroups.get(y).getValue())
                        defValueMU [y] = 1;
                }
                else if (Math.abs(middleHistGroups.get(y).getIdx() - upperHistGroups.get(y).getIdx()) <= 20){
                    if (middleHistGroups.get(y).getValue() > upperHistGroups.get(y).getValue())
                        defValueMU [y] = 1;
                }
                else Log.d("we can not compare the middel group number " + middleHistGroups.get(y), "with the upper group number " + upperHistGroups.get(y));
            }

       /* }
        else {
            for (int y = 0; y < upperHistGroups.size(); y++){
                if (middleHistGroups.get(y).getIdx() == upperHistGroups.get(y).getIdx()){
                    if (middleHistGroups.get(y).getValue() > upperHistGroups.get(y).getValue())
                        defValueMU [y] = 1;
                }
                else if (Math.abs(middleHistGroups.get(y).getIdx() - upperHistGroups.get(y).getIdx()) <= 20){
                    if (middleHistGroups.get(y).getValue() > upperHistGroups.get(y).getValue())
                        defValueMU [y] = 1;
                }
                else Log.d("we can not compare the middel group number " + middleHistGroups.get(y), "with the upper group number " + upperHistGroups.get(y));
            }*/
       // }


        //the meddel with the bottom side of the cross
      /*  if (middleHistGroups.size() > bottomHistGroups.size()){

        defValueMB = 1;
        /*for (int y = 0; y < middleHistGroups.size(); y++){
            if (middleHistGroups.get(y).getIdx() == bottomHistGroups.get(y).getIdx()){
                if (middleHistGroups.get(y).getValue() > bottomHistGroups.get(y).getValue())
                    defValueMB [y] = 1;
            }
            else if (Math.abs(middleHistGroups.get(y).getIdx() - bottomHistGroups.get(y).getIdx()) <= 20){
                if (middleHistGroups.get(y).getValue() > bottomHistGroups.get(y).getValue())
                    defValueMB [y] = 1;
            }
            else Log.d("we can not compare the middel group number " + y, "with the bottom group number " + y);
        }}
        else {
            for (int y = 0; y < bottomHistGroups.size(); y++){
                if (middleHistGroups.get(y).getIdx() == bottomHistGroups.get(y).getIdx()){
                    if (middleHistGroups.get(y).getValue() > bottomHistGroups.get(y).getValue())
                        defValueMB [y] = 1;
                }
                else if (Math.abs(middleHistGroups.get(y).getIdx() - bottomHistGroups.get(y).getIdx()) <= 20){
                    if (middleHistGroups.get(y).getValue() > bottomHistGroups.get(y).getValue())
                        defValueMB [y] = 1;
                }
                else Log.d("we can not compare the middel group number " + y, "with the bottom group number " + y);
            }*/
        }

        //the meddel with the left side of the cross
       /* if (middleHistGroups.size() > leftHistGroups.size()){

            defValueML = 1;
        /*for (int y = 0; y < middleHistGroups.size(); y++){
            if (middleHistGroups.get(y).getIdx() == leftHistGroups.get(y).getIdx()){
                if (middleHistGroups.get(y).getValue() > leftHistGroups.get(y).getValue())
                    defValueML [y] = 1;
            }
            else if (Math.abs(middleHistGroups.get(y).getIdx() - leftHistGroups.get(y).getIdx()) <= 30){
                if (middleHistGroups.get(y).getValue() > leftHistGroups.get(y).getValue())
                    defValueML [y] = 1;
            }
            else Log.d("we can not compare the middel group number " + y, "with the left group number " + y);
        }}
        else {
            for (int y = 0; y < leftHistGroups.size(); y++){
                if (middleHistGroups.get(y).getIdx() == leftHistGroups.get(y).getIdx()){
                    if (middleHistGroups.get(y).getValue() > leftHistGroups.get(y).getValue())
                        defValueML [y] = 1;
                }
                else if (Math.abs(middleHistGroups.get(y).getIdx() - leftHistGroups.get(y).getIdx()) <= 30){
                    if (middleHistGroups.get(y).getValue() > leftHistGroups.get(y).getValue())
                        defValueML [y] = 1;
                }
                else Log.d("we can not compare the middel group number " + y, "with the left group number " + y);
            }*/
     //   }

        //the meddel with the right side of the cross
      /*  if (middleHistGroups.size() > rightHistGroups.size()){

            defValueMR = 1;
        /*for (int y = 0; y < middleHistGroups.size(); y++){
            if (middleHistGroups.get(y).getIdx() == rightHistGroups.get(y).getIdx()){
                if (middleHistGroups.get(y).getValue() > rightHistGroups.get(y).getValue())
                    defValueMR [y] = 1;
            }
            else if (Math.abs(middleHistGroups.get(y).getIdx() - rightHistGroups.get(y).getIdx()) <= 30){
                if (middleHistGroups.get(y).getValue() > rightHistGroups.get(y).getValue())
                    defValueMR [y] = 1;
            }
            else Log.d("we can not compare the middel group number " + middleHistGroups.get(y), "with the right group number " + rightHistGroups.get(y));
        }}
        else {
            for (int y = 0; y < rightHistGroups.size(); y++){
                if (middleHistGroups.get(y).getIdx() == rightHistGroups.get(y).getIdx()){
                    if (middleHistGroups.get(y).getValue() > rightHistGroups.get(y).getValue())
                        defValueMR [y] = 1;
                }
                else if (Math.abs(middleHistGroups.get(y).getIdx() - rightHistGroups.get(y).getIdx()) <= 30){
                    if (middleHistGroups.get(y).getValue() > rightHistGroups.get(y).getValue())
                        defValueMR [y] = 1;
                }
                else Log.d("we can not compare the middel group number " + middleHistGroups.get(y), "with the right group number " + rightHistGroups.get(y));
            }*/
        }

        /*int rightvalueMB = 0;
        int rightvalueMU = 0;
        int rightvalueMR = 0;
        int rightvalueML = 0;
       for (int s = 0; s < 256; s++){
           if (defValueMB [s] == 1){
            rightvalueMB = rightvalueMB + 1;
           }
           if (defValueMU [s] == 1){
               rightvalueMU = rightvalueMU + 1;
           }
           if (defValueMR [s] == 1){
               rightvalueMR = rightvalueMR + 1;
           }
           if (defValueML [s] == 1){
               rightvalueML = rightvalueML + 1;
           }

       }*/

       /* if ((defValueMB == 1) && (defValueML == 1) && (defValueMR == 1) && (defValueMU == 1)){
            return 1;

        }
        else return 0;

        /*if ((middleHistGroups.size() < upperHistGroups.size()) && (middleHistGroups.size() < bottomHistGroups.size()) && (middleHistGroups.size() < leftHistGroups.size()) && (middleHistGroups.size() < rightHistGroups.size())) {
            for (int i = 0; i < middleHistGroups.size(); i++) {
                /* ((middleHistGroups.get(i).getValue() > upperHistGroups.get(i).getValue()) && (middleHistGroups.get(i).getValue() > bottomHistGroups.get(i).getValue()) && (middleHistGroups.get(i).getValue() > leftHistGroups.get(i).getValue()) && (middleHistGroups.get(i).getValue() > rightHistGroups.get(i).getValue())) {
                    return 1;
                } else break;*/
              /*  int IdxDefMU = Math.abs(middleHistGroups.get(i).getIdx() - upperHistGroups.get(i).getIdx());
                int IdxDefMB = Math.abs(middleHistGroups.get(i).getIdx() - bottomHistGroups.get(i).getIdx());
                int IdxDefML = Math.abs(middleHistGroups.get(i).getIdx() - leftHistGroups.get(i).getIdx());
                int IdxDefMR = Math.abs(middleHistGroups.get(i).getIdx() - rightHistGroups.get(i).getIdx());

                if (IdxDefMU < 5){
                    defValueMU [i] = (middleHistGroups.get(i).getValue() > upperHistGroups.get(i).getValue());
                }

                if (IdxDefMB < 5){
                    defValueMB [i] = (middleHistGroups.get(i).getValue() > bottomHistGroups.get(i).getValue());
                }

                if (IdxDefML < 5){
                    defValueML [i] = (middleHistGroups.get(i).getValue() > leftHistGroups.get(i).getValue());
                }

                if (IdxDefMR < 5){
                    defValueMR [i] = (middleHistGroups.get(i).getValue() > rightHistGroups.get(i).getValue());
                }

            }
            int rightvalueMB = defValueMB.length;
            int rightvalueMU = defValueMU.length;
            int rightvalueMR = defValueMR.length;
            int rightvalueML = defValueML.length;

            if ((rightvalueMB > 10) && (rightvalueMU > 10) && (rightvalueMR > 10) && (rightvalueML > 10)){
                return 1;

            }
            else return 0;
        }*/
       /* else {
            int min = middleHistGroups.size();

            if (min > upperHistGroups.size()){
                min = upperHistGroups.size();
            }
            else if (min > bottomHistGroups.size()){
                min = bottomHistGroups.size();
            }
            else if (min > leftHistGroups.size()){
                min = leftHistGroups.size();
            }
            else if (min > rightHistGroups.size()){
                min = rightHistGroups.size();
            }
            else{
                min = middleHistGroups.size();
            }

            for (int i = 0; i < min; i++) {
                int prevIdxDefMU = Math.abs(middleHistGroups.get(i).getPrevIdx() - upperHistGroups.get(i).getPrevIdx());
                int prevIdxDefMB = Math.abs(middleHistGroups.get(i).getPrevIdx() - bottomHistGroups.get(i).getPrevIdx());
                int prevIdxDefML = Math.abs(middleHistGroups.get(i).getPrevIdx() - leftHistGroups.get(i).getPrevIdx());
                int prevIdxDefMR = Math.abs(middleHistGroups.get(i).getPrevIdx() - rightHistGroups.get(i).getPrevIdx());

                if (prevIdxDefMU < 5){
                    defValueMU [i] = (middleHistGroups.get(i).getValue() > upperHistGroups.get(i).getValue());
                }

                if (prevIdxDefMB < 5){
                    defValueMB [i] = (middleHistGroups.get(i).getValue() > bottomHistGroups.get(i).getValue());
                }

                if (prevIdxDefML < 5){
                    defValueML [i] = (middleHistGroups.get(i).getValue() > leftHistGroups.get(i).getValue());
                }

                if (prevIdxDefMR < 5){
                    defValueMR [i] = (middleHistGroups.get(i).getValue() > rightHistGroups.get(i).getValue());
                }

            }
            int rightvalueMB = defValueMB.length;
            int rightvalueMU = defValueMU.length;
            int rightvalueMR = defValueMR.length;
            int rightvalueML = defValueML.length;

            if ((rightvalueMB > 10) && (rightvalueMU > 10) && (rightvalueMR > 10) && (rightvalueML > 10)){
                return 1;
            }
            else return 0;
        }*/


        /*if ((cameraHistGroups.size() > temHistGroups.size()) && (cameraHistGroups.size() - temHistGroups.size() <= 10)  ){
            for (int y = 0; y < temHistGroups.size(); y++){
                valueDifferences [y] = cameraHistGroups.get(y).getValue() - temHistGroups.get(y).getValue();
                idxDifferences   [y] = cameraHistGroups.get(y).getIdx() - temHistGroups.get(y).getIdx();

                if ((valueDifferences [y]  <= 5) && (valueDifferences [y] >= 0)){
                    closeValues [y] = valueDifferences [y];
                    closeIdx    [y] = idxDifferences   [y];
                }
            }
        }

        else if ((cameraHistGroups.size() < temHistGroups.size()) && (temHistGroups.size() - cameraHistGroups.size() <= 10) && temHistGroups.size() - cameraHistGroups.size() >= 0){
            for (int y = 0; y < cameraHistGroups.size(); y++){
                valueDifferences [y] = cameraHistGroups.get(y).getValue() - temHistGroups.get(y).getValue();
                idxDifferences   [y] = cameraHistGroups.get(y).getIdx() - temHistGroups.get(y).getIdx();

                if ((valueDifferences [y]  <= 5) && (valueDifferences [y] >= 0)){
                    closeValues [y] = valueDifferences [y];
                    closeIdx    [y] = idxDifferences   [y];
                }
            }
        }

        else {
            return false;
        }

        for (int z = 0; z <= valueDifferences.length; z++){
            Log.d("the Value Differences are: ", " "  + valueDifferences[z]);
        }

        return true;*/


    /*}

    public static int comparePeaksB(Mat middle, Mat upper, Mat bottom, Mat left, Mat right) {

        ArrayList<Group> middleHistGroups = GetHistoOpenCV.findAllMax(middle);
        ArrayList<Group> upperHistGroups  = GetHistoOpenCV.findAllMax(upper);
        ArrayList<Group> bottomHistGroups = GetHistoOpenCV.findAllMax(bottom);
        ArrayList<Group> leftHistGroups   = GetHistoOpenCV.findAllMax(left);
        ArrayList<Group> rightHistGroups  = GetHistoOpenCV.findAllMax(right);

        int defValueMU ;
        int defValueMB ;
        int defValueML ;
        int defValueMR ;

        Log.d("Comparator","the size of the middleHistGroups for blue histogram is:  " + middleHistGroups.size());
        Log.d("Comparator","the size of the upperHistGroups for blue histogram is:   " + upperHistGroups.size());
        Log.d("Comparator","the size of the bottomHistGroups for blue histogram is:   " + bottomHistGroups.size());
        Log.d("Comparator","the size of the leftHistGroups for blue histogram is:  " + leftHistGroups.size());
        Log.d("Comparator","the size of the rightHistGroups for blue histogram is:  " + rightHistGroups.size());

        for (int n = 0; n < middleHistGroups.size(); n++) {
            Log.d("The middel B Idx " + middleHistGroups.get(n).getIdx(), "has the value:  " + middleHistGroups.get(n).getValue());
        }
        for (int n = 0; n < upperHistGroups.size(); n++) {
            Log.d("The upper B Idx " + upperHistGroups.get(n).getIdx(), "has the value:  " + upperHistGroups.get(n).getValue());
        }
        for (int n = 0; n < bottomHistGroups.size(); n++) {
            Log.d("The bottom B Idx " + bottomHistGroups.get(n).getIdx(), "has the value:  " + bottomHistGroups.get(n).getValue());
        }
        for (int n = 0; n < rightHistGroups.size(); n++) {
            Log.d("The right B Idx " + rightHistGroups.get(n).getIdx(), "has the value:  " + rightHistGroups.get(n).getValue());
        }
        for (int n = 0; n < leftHistGroups.size(); n++) {
            Log.d("The left B Idx " + leftHistGroups.get(n).getIdx(), "has the value:  " + leftHistGroups.get(n).getValue());
        }

        //the meddel with the upper side of the cross
        if (middleHistGroups.size() <= upperHistGroups.size()){
            defValueMU = 1;
        }else defValueMU = 0;

        //the meddel with the bottom side of the cross
        if (middleHistGroups.size() <= bottomHistGroups.size()){
            defValueMB = 1;
        }else defValueMB = 0;

        //the meddel with the left side of the cross
        if (middleHistGroups.size() <= leftHistGroups.size()){
            defValueML = 1;
        }else defValueML = 0;

        //the meddel with the right side of the cross
        if (middleHistGroups.size() <= rightHistGroups.size()){
            defValueMR = 1;
        }else defValueMR = 0;


        if ((defValueMB == 1) && (defValueML == 1) && (defValueMR == 1) && (defValueMU == 1)){
            return 1;

        }
        else return 0;

    }}*/




