package com.qcon.testcrossdemo;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import android.hardware.Camera.Size;
import android.view.MotionEvent;
import android.view.SubMenu;
import android.view.SurfaceView;
import android.view.ViewConfiguration;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.Toast;

import android.annotation.SuppressLint;

public class MainActivity extends Activity implements CvCameraViewListener2, View.OnTouchListener {

    private static final String TAG = "TestCrossDemo::Main";

    private OpenCvCameraView mOpenCvCameraView;
    
    private MenuItem[] mEffectMenuItems;
    private SubMenu mColorEffectsMenu;
    
    private List<String> mFlashList;
    private MenuItem[] mFlashMenuItems;
    private SubMenu mFlashMenu;
    
    private List<String> mFocusList;
    private MenuItem[] mFocusMenuItems;
    private SubMenu mFocusMenu;
    
    private List<Size> mResolutionList;
    private MenuItem[] mResolutionMenuItems;
    private SubMenu mResolutionMenu;

    private List<String> mOptionList;
    private MenuItem[] mOptionMenuItems;
    private SubMenu mOptionMenu;

    private List<String> mOptionDebugList;
    private MenuItem[] mOptionDebugMenuItems;
    private SubMenu mOptionDebugMenu;

    private boolean writeDebugInformation = false;
    private boolean writeDebugImage = false;

    int heightPreview = 0;
    int widthPreview = 0;

    boolean bTakePreviewPicture = false;
    boolean bWithFocus = false;

    int detectionMethode = 5;

    boolean firstrun = true;
    private Mat mCameraRGBA = null;
    private Mat mResult = null;
    private Mat mResultMask = null;
    private Mat mMarkerFound = null;
    private Mat mMarkerFoundMask = null;
    private Mat mMarkerNotFound = null;
    private Mat mMarkerNotFoundMask = null;

    public Mat createMask (Mat sprite){
        Mat mCurrentMask = new Mat(sprite.width(),sprite.height(),24);
        double f[] = {1,1,1,0};
        double e[] = {0,0,0,0};
        for(int y = 0; y < (int)(sprite.rows()) ; ++y)
        {
            for(int x = 0; x < (int)(sprite.cols()) ; ++x)
            {
                double info[] = sprite.get(y, x);
                if(info[3]>0) //rude but this is what I need
                {
                    mCurrentMask.put(y, x, f);
                }
                else mCurrentMask.put(y, x, e);
            }
        }
        return mCurrentMask;
    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                    mOpenCvCameraView.setOnTouchListener(MainActivity.this);

                    if (  mMarkerFound == null || mMarkerNotFound == null) {
                        try {
                            mMarkerFound = Utils.loadResource(getApplicationContext(), R.drawable.haken);
                            Imgproc.cvtColor(mMarkerFound, mMarkerFound, Imgproc.COLOR_RGBA2BGRA);
                            mMarkerFoundMask = createMask(mMarkerFound);
                            mMarkerNotFound = Utils.loadResource(getApplicationContext(), R.drawable.kreuz);
                            Imgproc.cvtColor(mMarkerNotFound, mMarkerNotFound, Imgproc.COLOR_RGBA2BGRA);
                            mMarkerNotFoundMask = createMask(mMarkerNotFound);
                        } catch (java.io.IOException e) {

                        }
                    }
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_main);

        mOpenCvCameraView = (OpenCvCameraView) findViewById(R.id.activity_java_surface_view);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);

        ImageButton button = (ImageButton) findViewById(R.id.alternate_menu_button);
        if (!ViewConfiguration.get(this).hasPermanentMenuKey()) {

            button.setVisibility(View.VISIBLE);
            button.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    Log.e(TAG, "called openOptionsMenu");
                    openOptionsMenu();

                }
            });
        } else {
            button.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }

        if (firstrun){
            dispatchTakePictureIntent();
            firstrun = false;
        }
    }

    public void setHighestResolution(){
        mResolutionList = mOpenCvCameraView.getPreviewResolutionList();

        Collections.sort(mResolutionList, new Comparator<Size>() {
            @Override
            public int compare(Size p1, Size p2) {
                return p1.width - p2.width;
            }
        } );

        Size test = mResolutionList.get(0);
        Size testMax = mResolutionList.get(mResolutionList.size()-1);
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
        heightPreview = height;
        widthPreview = width;
    }

    public void onCameraViewStopped() {
    }

    static final int REQUEST_IMAGE_CAPTURE = 1;

    private void dispatchTakePictureIntent() {

        if(false) {
            detect();
        }else {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            File file = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS), "lastScanCross.jpg");

            Uri imgUri = Uri.fromFile(file);

            Intent intent = takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {

        mCameraRGBA = inputFrame.rgba();

        if (mResult != null) {
            int xSubMat = mCameraRGBA.cols()/2 - mResult.cols()/2;
            int ySubMat = mCameraRGBA.rows()/2 - mResult.rows()/2;
            Mat subMat = mCameraRGBA.submat( ySubMat,  ySubMat + mResult.rows(), xSubMat, xSubMat + mResult.cols());
            mResult.copyTo(subMat,mResultMask);
        }

        if ( bTakePreviewPicture) {
            bTakePreviewPicture = false;
            mCameraRGBA = inputFrame.rgba();

            Imgproc.cvtColor(mCameraRGBA, mCameraRGBA, Imgproc.COLOR_RGBA2BGR);
            Log.e("christoph", String.format("writeDebugInformation %b - writeDebugImage %b", writeDebugInformation, writeDebugImage ));
            CrossDetetctor detector = new CrossDetetctor(getApplicationContext(), detectionMethode, 600, writeDebugInformation, writeDebugImage);
            final int ret = detector.findConture( mCameraRGBA);

            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (ret == 1)
                        Toast.makeText(MainActivity.this, "Marker gefunden - Methode " + detectionMethode, Toast.LENGTH_LONG).show();
                    else if (ret == 0)
                        Toast.makeText(MainActivity.this, "Keinen Marker gefunden - Methode " + detectionMethode, Toast.LENGTH_LONG).show();
                    else
                        Toast.makeText(MainActivity.this, "Kein Objekt gefunden", Toast.LENGTH_LONG).show();

                }
            });



        }
        /*
        mCameraRGBA = inputFrame.rgba();
        Mat test = new Mat();
        //convert the image to black and white
        Imgproc.cvtColor(mCameraRGBA, mCameraRGBA, Imgproc.COLOR_RGBA2GRAY);
        //Imgproc.cvtColor(imgSource, imgSource, Imgproc.COLOR_BGR2GRAY);
        Imgproc.GaussianBlur(mCameraRGBA, mCameraRGBA, new org.opencv.core.Size(15, 15), 150);
        //Imgproc.Canny(mCameraRGBA, test, 50, 100);
        //convert the image to black and white does (8 bit)
        //Imgproc.Canny(imgSource, imgSource, 50, 50);

        Imgproc.cvtColor(mCameraRGBA, mCameraRGBA, Imgproc.COLOR_GRAY2RGBA);
        return mCameraRGBA;
        */
        return mCameraRGBA;
    }

    @Override
    public void openOptionsMenu() {

        Configuration config = getResources().getConfiguration();

        if ((config.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK)
                > Configuration.SCREENLAYOUT_SIZE_LARGE) {
            Log.e(TAG, "a openOptionsMenu");
            int originalScreenLayout = config.screenLayout;
            config.screenLayout = Configuration.SCREENLAYOUT_SIZE_LARGE;
            super.openOptionsMenu();
            config.screenLayout = originalScreenLayout;

        } else {
            Log.e(TAG, "b  openOptionsMenu");
            super.openOptionsMenu();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        mOptionMenu = menu.addSubMenu("Option");
        mOptionList = new ArrayList<String>();
        mOptionList.add("Focus on Tap");
        mOptionList.add("Detection Mode 1");
        mOptionList.add("Detection Mode 2");
        mOptionList.add("Detection Mode 3");
        mOptionList.add("Detection Mode 4");
        mOptionList.add("Detection Mode 5");
        mOptionMenuItems = new MenuItem[mOptionList.size()];

        ListIterator<String> optionItr = mOptionList.listIterator();
        int idx = 0;
        while (optionItr.hasNext()) {
            String element = optionItr.next();
            mOptionMenuItems[idx] = mOptionMenu.add(4, idx, Menu.NONE, element);
            idx++;
        }

        mOptionDebugMenu = menu.addSubMenu("Debug");
        mOptionDebugList = new ArrayList<String>();
        mOptionDebugList.add("Debug ON");
        mOptionDebugList.add("Debug OFF");
        mOptionDebugList.add("Only Debug Information ON");
        mOptionDebugList.add("Only Debug Information OFF");
        mOptionDebugList.add("Only Debug Image ON");
        mOptionDebugList.add("Only Debug Image OFF");
        mOptionDebugMenuItems = new MenuItem[mOptionDebugList.size()];

        ListIterator<String> optionDebugItr = mOptionDebugList.listIterator();
        idx = 0;
        while (optionDebugItr.hasNext()) {
            String element = optionDebugItr.next();
            mOptionDebugMenuItems[idx] = mOptionDebugMenu.add(5, idx, Menu.NONE, element);
            idx++;
        }
        /*
        mFocusMenu = menu.addSubMenu("Focus");
        mFocusList = mOpenCvCameraView.getFocusList();
        mFocusMenuItems = new MenuItem[mFocusList.size()];

        ListIterator<String> focusItr = mFocusList.listIterator();
        idx = 0;
        while (focusItr.hasNext()) {
            String element = focusItr.next();
            mFocusMenuItems[idx] = mFocusMenu.add(1, idx, Menu.NONE, element);
            idx++;
        }


        mFlashList = mOpenCvCameraView.getFlashList();
        if(mFlashList != null) {
            mFlashMenu = menu.addSubMenu("Flash");
            mFlashList = mOpenCvCameraView.getFlashList();
            mFlashMenuItems = new MenuItem[mFlashList.size()];

            ListIterator<String> flashItr = mFlashList.listIterator();
            idx = 0;
            while (flashItr.hasNext()) {
                String element = flashItr.next();
                mFlashMenuItems[idx] = mFlashMenu.add(2, idx, Menu.NONE, element);
                idx++;
            }
        }
        
        mResolutionMenu = menu.addSubMenu("Resolution");
        mResolutionList = mOpenCvCameraView.getPreviewResolutionList();
        mResolutionMenuItems = new MenuItem[mResolutionList.size()];

        ListIterator<Size> resolutionItr = mResolutionList.listIterator();
        idx = 0;
        while (resolutionItr.hasNext()) {
            Size element = resolutionItr.next();
            mResolutionMenuItems[idx] = mResolutionMenu.add(3, idx, Menu.NONE,
                    Integer.valueOf(element.width).toString() + "x" + Integer.valueOf(element.height).toString());
            idx++;
        }

        */

        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(TAG, "called onOptionsItemSelected; selected item: " + item);
        int id = item.getItemId();

        if (item.getGroupId() == 1) {
            //String focus = mFocusList.get(id);
            mOpenCvCameraView.setFocusMethod((String) item.getTitle());
            Toast.makeText(this, mOpenCvCameraView.getFocusMethod(), Toast.LENGTH_SHORT).show();
        } else  if (item.getGroupId() == 5) {

            if (id == 0) {
                writeDebugInformation = true;
                writeDebugImage = true;
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
            }
            else  if (id == 1) {
                writeDebugInformation = false;
                writeDebugImage = false;
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
            }else  if (id == 2 ){
                writeDebugInformation = true;
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
            }else  if (id == 3) {
                writeDebugInformation = false;
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
            }else  if (id == 4) {
                writeDebugImage = true;
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
            }else  if (id == 5) {
                writeDebugImage = false;
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
            }

        } if (item.getGroupId() == 2) {
            //Size resolution = mResolutionList.get(id);
            mOpenCvCameraView.setFlashMethod((String) item.getTitle());
            Toast.makeText(this, "Flash " + mOpenCvCameraView.getFlashMethod(), Toast.LENGTH_SHORT).show();
        }else if (item.getGroupId() == 3) {
            Size resolution = mResolutionList.get(id);
            mOpenCvCameraView.setPreviewResolution(resolution);
            resolution = mOpenCvCameraView.getPreviewResolution();
            String caption = Integer.valueOf(resolution.width).toString() + "x" + Integer.valueOf(resolution.height).toString();
            Toast.makeText(this, caption, Toast.LENGTH_SHORT).show();
        } else if (item.getGroupId() == 4) {
            if (id == 0) {
                bWithFocus = !bWithFocus;
                Toast.makeText(this, item.getTitle() + (bWithFocus ? " on" : " off"), Toast.LENGTH_SHORT).show();
            }
            else {
                detectionMethode = id;
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
            }

        }

        return true;
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Log.i(TAG, "onTouch event");

        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:

                if (false && bWithFocus){ //deactivated
                    float x = event.getX();
                    float y = event.getY();

                    takePictureWithAutoFocus(x,y);
                }else{
                    dispatchTakePictureIntent();
                    //bTakePreviewPicture = true;
                }
                //mOpenCvCameraView.setResolution( tmpSizes.get(2));
                //mOpenCvCameraView.takePicture("test.jpg");
                break;
            default:
                break;
        }
        return true;
    }

    void takePictureWithAutoFocus( float x, float y){

        Rect touchRect = new Rect(
                (int)(x - 100),
                (int)(y - 100),
                (int)(x + 100),
                (int)(y + 100));

        final Rect targetFocusRect = new Rect(
                touchRect.left * 2000/this.widthPreview - 1000,
                touchRect.top * 2000/this.heightPreview - 1000,
                touchRect.right * 2000/this.widthPreview - 1000,
                touchRect.bottom * 2000/this.heightPreview - 1000);

        Camera.AutoFocusCallback myAutoFocusCallback = new Camera.AutoFocusCallback(){

            @Override
            public void onAutoFocus(boolean arg0, Camera arg1) {
                if (arg0){
                    mOpenCvCameraView.cancelAutoFocus();
                    bTakePreviewPicture = true;
                }
            }
        };

        mOpenCvCameraView.setFocus(touchRect,myAutoFocusCallback);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case  REQUEST_IMAGE_CAPTURE:
                if (resultCode == RESULT_OK) {
                    detect();
                }
                break;
        }


    }
    protected void detect() {

        int ret = 0;
        File file = new File(Environment.getExternalStoragePublicDirectory(
        Environment.DIRECTORY_DOWNLOADS), "lastScanCross.jpg");

        // this should be in BGR format according to the
        // documentation.
        Mat image = Imgcodecs.imread(file.getAbsolutePath());


        //Imgproc.cvtColor(mCameraRGBA, mCameraRGBA, Imgproc.COLOR_RGBA2BGR);

        try {
            CrossDetetctor detector = new CrossDetetctor(getApplicationContext(), detectionMethode, 600, writeDebugInformation, writeDebugImage);
            ret = detector.analyse(image);
        } catch(Exception e){
            ret = -2;
        }
        final int finalRet = ret;

        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (finalRet == 1) {
                    Toast.makeText(MainActivity.this, "Marker gefunden - Methode " + detectionMethode, Toast.LENGTH_SHORT).show();
                    mResult = mMarkerFound.clone();
                    mResultMask = mMarkerFoundMask.clone();
                } else {

                    mResult = mMarkerNotFound.clone();
                    mResultMask = mMarkerNotFoundMask.clone();

                    if (finalRet == 0) {
                        Toast.makeText(MainActivity.this, "Keinen Marker gefunden - Methode " + detectionMethode, Toast.LENGTH_SHORT).show();
                    } else if (finalRet == -2 ) {
                        Toast.makeText(MainActivity.this, "Es ist ein Fehler aufgetreten - Methode " + detectionMethode, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MainActivity.this, "Kein Objekt gefunden", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

    }
}