package com.qcon.testcrossdemo;

//import android.support.annotation.NonNull;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by ck on 10.09.15.
 */
public class RowInformation {

    List<Integer> row;
    List<Integer> delta;
    List<Integer> deltaSharp;
    List<Integer> peaks;
    List<Integer> newPeaks;


    String addInfo;
    int channel;
    int minDelta;
    int countInterval;

    public RowInformation(List<Integer> row, int channel, int minDelta, String addInfo){
        this.row = row;
        this.addInfo = addInfo;
        this.channel = channel;
        this.minDelta = minDelta;
        newPeaks = new ArrayList<Integer>();
        
        /*
        calculateDeltas();
        sharpDeltas();
        calculatePeaks();
        
        countInterval = countIntervals();
        */
    }

    
    public int countIntervals(){
    	int i = 0;
    	int last = 0;
    	int right = 0;
    	int left = 0;
    	int minMid = 0;

    	int countInterval = 0;
    	int loseInterval = 0;
    	int interpolateInterval = 0;
    	newPeaks = new ArrayList<Integer>();
    	
    	boolean firstInterval = true;

    	while (i < this.row.size()- 7){
    		 left = row.get(i);
    		 
    		 if (row.get(i+5) > row.get(i+6))
 		        last = i+5;
 		     else if  (row.get(i+6) >= row.get(i+7))
 		        last =i+6;
 		     else
 		        last =i+7;
  
    		 right=row.get(last);
    		 minMid =  Collections.min(row.subList(i+1, last));
    				 
    		 
    		 if (firstInterval){
 		        if (left >= row.get(i+1) &&  left > minMid + minDelta &&  right >  minMid + minDelta){
 		            firstInterval = false;
 		            newPeaks.add(1);
	            	newPeaks.add(0);
	            	newPeaks.add(0);
	            	newPeaks.add(-1);
	            	newPeaks.add(0);
	            	
 		            if (last-i == 6){
 		            	newPeaks.add(0);
 		            }
	                if (last-i == 7){
 		            	newPeaks.add(0);
 		            }
 		        
 		            countInterval+=1;
 		            interpolateInterval = interpolateInterval + 1 + loseInterval;
 		            loseInterval=0;
 		            i=last;
 		        }
 		        else{
 		        	newPeaks.add(0);
 		            i+=1;
 		        }
    		 }
 		    else{
 		        if ( left > minMid + minDelta &&  right >  minMid + minDelta){
 		            newPeaks.add(1);
	            	newPeaks.add(0);
	            	newPeaks.add(0);
	            	newPeaks.add(-1);
	            	newPeaks.add(0);
	            	
		            if (last-i == 6){
		            	newPeaks.add(0);
		            }
	                if (last-i == 7){
		            	newPeaks.add(0);
		            }
 		            countInterval+=1;
 		            interpolateInterval += 1 + loseInterval;
 		            loseInterval=0;
 		        }
 		        else{ 
 		            newPeaks.add(9);
	            	newPeaks.add(9);
	            	newPeaks.add(9);
	            	newPeaks.add(9);
	            	newPeaks.add(9);
	            	
		            if (last-i == 6){
		            	newPeaks.add(9);
		            }
	                if (last-i == 7){
		            	newPeaks.add(9);
		            }
 		            loseInterval+=1;
 		        }
 		       i=last;
 		    }
    	}

        countInterval = interpolateInterval;

    	return interpolateInterval;
    }
    
    public boolean calculatePeaks(){

        peaks = new ArrayList<Integer>();

        int x;
        //min peaks
        for (int i = 0; i < row.size() ; i++) {
            peaks.add(0);
            if (i < 2 ||  i > row.size() - 3)
                continue;

            x = row.get(i);
            if (row.get(i-2) > x + minDelta && row.get(i+2) > x + minDelta && row.get(i-1) >= x && row.get(i+1) >= x){
                peaks.set(i, -x);
                peaks.add(0);
                peaks.add(0);
                i+=2;
            }
        }

        //max peaks
        for (int i = 0; i < row.size(); i++) {
            if (i < 2 ||  i > row.size() - 3)
                continue;

            x = row.get(i);
            if (row.get(i-2) < x - minDelta && row.get(i+2) < x - minDelta && row.get(i-1) <= x && row.get(i+1) <= x){
                peaks.set(i, x);
                i+=2;
            }
        }

        return true;
    }

    public boolean calculateDeltas(){
        delta = new ArrayList<Integer>();
        for (int i = 1; i < row.size(); i++) {
            delta.add(row.get(i-1) - row.get(i));
        }
        return true;
    }

    public int countPeakMax() {
        int countMax = 0;

        for (int i = 0; i < peaks.size(); i++) {
            if (peaks.get(i) > 0){
                countMax++;
            }
        }

        return countMax;
    }

    public int countPeakMin() {
        int countMax = 0;

        for (int i = 0; i < peaks.size(); i++) {
            if (peaks.get(i) < 0){
                countMax++;
            }
        }

        return countMax;
    }

    /**
     *  1 ,2 ,3 ,4 ,5 ,7 -4 ,-6, -1 , -2
     * @return
     */
    public boolean sharpDeltas(){
        deltaSharp = sharpHighLow(new ArrayList<Integer>(delta));
        return true;
    }

    public List<Integer> getListOfHighDelta(int countDelta, int minDelta) {
        List<Integer> ret = new ArrayList<Integer>(deltaSharp);

        Collections.sort(ret);

        int j = 0;
        while(j <  ret.size() && j < countDelta && ret.get(j) >= minDelta) {
            j++;
        }

        return ret.subList(0,j+1);
    }

    public List<Integer> getListOfLowDelta(int countDelta, int minDelta) {
        List<Integer> ret = new ArrayList<Integer>(deltaSharp);

        Collections.sort(ret);
        Collections.reverse(ret);

        int j = 0;
        while(j <  ret.size() && j < countDelta && ret.get(j) <= minDelta) {
            j++;
        }

        return ret.subList(0,j+1);
    }

    public List<Integer> sharpHighLow( List<Integer> lst){

        List<Integer> ret = new ArrayList<Integer>();
        for (int i = 0; i < lst.size(); i++) {
            ret.add(0);
        }

        int max = 1;
        while(max > 0) {
            max = Collections.max(lst);
            if (max > 0) {
                int idx = lst.indexOf(max);
                ret.set(idx,lst.get(idx));
                lst.set(idx, 0);
                if (idx > 0 && lst.get(idx - 1) > 0 ) {
                    ret.set(idx, ret.get(idx) + lst.get(idx - 1));
                    lst.set(idx - 1, 0);
                }
                if (idx < lst.size() - 1  && lst.get(idx + 1) > 0 ) {
                    ret.set(idx, ret.get(idx) + lst.get(idx + 1));
                    lst.set(idx + 1, 0);
                }
            }
        }

        int min = -1;
        while(min < 0) {
            min = Collections.min(lst);
            if (min < 0) {
                int idx = lst.indexOf(min);
                ret.set(idx,lst.get(idx));
                lst.set(idx, 0);
                if (idx > 0 && lst.get(idx - 1) < 0 ) {
                    ret.set(idx, ret.get(idx) + lst.get(idx - 1));
                    lst.set(idx - 1, 0);
                }
                if (idx < lst.size() - 1  && lst.get(idx + 1) < 0 ) {
                    ret.set(idx, ret.get(idx) + lst.get(idx + 1));
                    lst.set(idx + 1, 0);
                }
            }
        }

        return ret;
    }

    public String writeRowInformation(){
        String out = new String();
        	
        out = out.concat(String.format("\nDelta %d, values %s C%d: ",minDelta, addInfo, channel));
        for (int j = 0; j <  row.size(); j++) {
            out = out.concat(String.format("%3d,", row.get(j)));
        }
        if (peaks != null){
	        out = out.concat(String.format("\nDelta %d,peaks %s C%d: ",minDelta, addInfo, channel));
	        for (int j = 0; j <  peaks.size(); j++) {
	            out = out.concat(String.format("%3d,", peaks.get(j)));
	        }
        }
        out = out.concat(String.format("\n %d Delta %d,interval %s C%d: ", countInterval, minDelta, addInfo, channel));
        for (int j = 0; j <  newPeaks.size(); j++) {
            out = out.concat(String.format("%3d,", newPeaks.get(j)));
        }
        
        /*
        out = out.concat(String.format("\nDelta %d: ", channel));
        for (int j = 0; j <  delta.size(); j++) {
            out = out.concat(String.format("%3d,", delta.get(j)));
        }
        out =out.concat(String.format("\nSharp %d: ", channel));
        for (int j = 0; j <  deltaSharp.size(); j++) {
            out = out.concat(String.format("%3d,", deltaSharp.get(j)));
        }
        */
        out = out.concat("\n");

        return out;
    }



}
